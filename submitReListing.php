<?php

/*
 * Filters data through the functions registered for "addListingForm" hook.
 * Passes the content through registered functions.
 */
 
print call_plugin("addListingForm",addListingForm());

function addListingForm(){
    include("config.php");
ob_start();     
$con=mysql_connect($host,$username,$password) or die("Could not connect. Please try again.");
mysql_select_db($database,$con);
mysql_query("SET NAMES utf8");
$mem_id=$_SESSION["re_mem_id"];
$qr="select * from $rememberTable where id='$mem_id'";
$result=mysql_query($qr);
$row=mysql_fetch_assoc($result);

$reqr1="select distinct category from listing where category<>''";
$resultre1=mysql_query($reqr1);

$reqr2="select distinct subcategory from listing where subcategory<>''";
$resultre2=mysql_query($reqr2);

$reqr3="select distinct activities from listing where activities<>''";
$resultre3=mysql_query($reqr3);

$reqr4="select distinct types from listing where types <>''";
$resultre4=mysql_query($reqr4);
 
?>
<div id='perimeter'>
<fieldset id='submitListingPage'>
<legend>
<b><?php print $relanguage_tags["Add Listing"];?></b>
</legend>
<form action='index.php' method='post' name='addReListingForm' class="form-horizontal">
<input type='hidden' id='isSubmitListingForm' name='isSubmitListingForm' value='1' />

<div class="form-group">
 <label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label" for="reCategory2"><span class='required_field' ><sup>*<sup></span><b><?php print __("Regions");?>:</b></label>
 <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> >
 <select name='category' id='reCategory2a' class="form-control">
 <option value='Select' selected='selected' ><?php print $relanguage_tags["Select"]; ?></option>
<?php 
while($allCategories=mysql_fetch_assoc($resultre1)){  ?>
<option value='<?php print __($allCategories['category']);?>' ><?php print __($allCategories['category']); ?></option>
<?php } ?>
 </select>
 </div>
 <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
</div>

<div id='subcategoriesSection2a'>
<div class="form-group">
 <label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label" for="reSubCategory2a"><span class='required_field' ><sup>*<sup></span><b><?php print __("Societies");?>:</b></label>
 <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> >
 <select name='subcategory' id='reCategorySubcategory2a' class="form-control">
 <option value='Select' selected='selected' ><?php print $relanguage_tags["Select"]; ?></option>
<?php 
while($allSubCategories=mysql_fetch_assoc($resultre2)){  ?>
<option value='<?php print __($allSubCategories['subcategory']);?>' ><?php print __($allSubCategories['subcategory']); ?></option>
<?php } ?>
 </select>
 </div>
 <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
</div>
    
    
</div>

<div id='activities'>
<div class="form-group">
 <label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label" for="activitiesSel"><span class='required_field' ><sup>*<sup></span><b><?php print __("Activities");?>:</b></label>
 <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> >
 <select name='activities' id='activitiesSel' class="form-control">
 <option value='Select' selected='selected' ><?php print $relanguage_tags["Select"]; ?></option>
<?php 
while($record=mysql_fetch_assoc($resultre3)){  ?>
<option value='<?php print __($record['activities']);?>' ><?php print __($record['activities']); ?></option>
<?php } ?>
 </select>
 </div>
 <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
</div>
    
    
</div>

<div id='types'>
<div class="form-group">
 <label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label" for="reSubCategory2a"><span class='required_field' ><sup>*<sup></span><b><?php print __("Types");?>:</b></label>
 <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> >
 <select name='types' id='typesSel' class="form-control">
 <option value='Select' selected='selected' ><?php print $relanguage_tags["Select"]; ?></option>
<?php 
while($record=mysql_fetch_assoc($resultre4)){  ?>
<option value='<?php print __($record['types']);?>' ><?php print __($record['types']); ?></option>
<?php } ?>
 </select>
 </div>
 <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
</div>
    
    
</div>

<div class="form-group" style='display:none;'>
<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label"><span class='required_field' ><sup>*<sup></span><b><?php print $relanguage_tags["Listing by"];?>:</b></label>
<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> >
<label class="radio"><?php print $relanguage_tags["Individual"];?> <input type="radio" name="relistingby" id='byIndividual' checked value="owner" /></label>
<label class="radio"><?php print $relanguage_tags["Other"];?><input type="radio" name="relistingby" id='reagentOther' value="reagent" /></label>
</div>
<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
</div>

 <div id='byOtherSection' style="display:none;">
 <div class="form-group">
  <label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label" for="byother"><span class='required_field' ><sup>*<sup></span><b><?php print $relanguage_tags["Please specify"];?>:</b></label>
  <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> ><input type='text' class="form-control" name='byother' id='byother' size='25' /></div>
  <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
 </div>
 </div>
 
<?php if($memtype==9){ ?>
<div class="form-group">
<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label"><span class='required_field' ><sup>*<sup></span><b><?php print $relanguage_tags["Listing status"];?>:</b> <font style="font-size:10px;">(<?php print $relanguage_tags["this option is visible to admin only"];?>)</font></label>
<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> >
<label class="radio"><?php print $relanguage_tags["Normal"];?> <input type="radio" name="listingexpire" id='listingNormal' checked value="normal" /></label>
<label class="radio"><?php print $relanguage_tags["Permanent"];?> <input type="radio" name="listingexpire" id="listingPermanent" value="permanent" /></label>
</div>
<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
<span id='listingStatus'></span>
</div>
<?php } ?>

<div id='priceField' style="display:none;" >
<div class="form-group">
<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label" for="reprice"><b><?php print $relanguage_tags["Price"];?> (<?php print $defaultCurrency; ?>):</b></label>
<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> >
<input type='text' class='textinput form-control' name='reprice' size='7' id='reprice' value='' onkeyup="if(this.value.match(/[^0-9\. ]/g)) { this.value = this.value.replace(/[^0-9\. ]/g, '');}" />
</div>
<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
</div>
</div>


<div class="form-group">
<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label" for="readdress"><b><?php print $relanguage_tags["Address"];?>:</b></label>
<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> >
<input type='text' class='textinput form-control' name='readdress' id='readdress' size='75' value='' />
</div>
<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
</div>

<div class="form-group">
<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label" for="recity"><span class='required_field' ><sup>*<sup></span><b><?php print $relanguage_tags["City"];?>:</b></label>
<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> ><input type='text' class='textinput form-control' name='recity' id='recity' value='<?php print $GLOBALS['vCity']; ?>'></div>
<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
</div>

<div class="form-group">
<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label" for="restate"><b><?php print $relanguage_tags["State"];?>:</b></label>
<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> ><input type='text' class='textinput form-control' name='restate'  id='restate' value='<?php print $GLOBALS['vRegion']; ?>'></div>
<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
</div>

<div class="form-group">
<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label" for="recountry"><b><?php print $relanguage_tags["Country"];?>:</b></label>
<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> ><input type='text' class='textinput form-control' name='recountry'  id='recountry' value='<?php print $GLOBALS['vCountry']; ?>'></div>
<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
</div>

<div class="form-group">
<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label" for="repostal"><b><?php print $relanguage_tags["Postal Code"];?>:</b></label>
<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> ><input type='text' class='textinput form-control' name='repostal' id='repostal' value=''></div>
<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
</div>

<div class="form-group">
<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label" for="customLocation"><b><?php print $relanguage_tags["Select custom location"];?>:</b></label>
<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> >
<input type='checkbox' id='customLocation'  />
<p class="help-block"><font style="font-size:10px;">(<?php print __("You will be able to choose the address by clicking on map");?>)</font></p>
</div>
<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
</div>

<div id='listinglatLong' style="display:none;">
<div class="form-group">
<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label" for="listingLatitude"><b><?php print $relanguage_tags["Latitude"];?>:</b></label>
<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> >
<input type='text' readonly name='latitude' class='textinput form-control' id='listingLatitude' />
</div>
<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
</div>

<div class="form-group">
<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label" for="listingLongitude"><b><?php print $relanguage_tags["Longitude"];?>:</b></label>
<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> >
<input type='text' readonly name='longitude' class='textinput form-control' id='listingLongitude' />
</div>
<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
</div>
</div>

<div id='addListingMap' style="width:100%; height:400px; display:none;"></div>
<br /><br />

<div class="form-group">
<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label" for="reheadline"><span class='required_field' ><sup>*<sup></span><b><?php print $relanguage_tags["Headline"];?>:</b></label>
<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9"><input type='text' class='textinput form-control' maxlength="150" name='reheadline' id='reheadline' style="width:90%;" value=''></div>
</div>

<div class="form-group">
<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label" for="redescription"><span class='required_field' ><sup>*<sup></span><b><?php print $relanguage_tags["Description"];?>:</b></label>
<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9"><textarea NAME="redescription" id='redescription' class='textinput4 form-control form-control'  style="width:90%;" ROWS=25></textarea></div>

</div>

<?php //if($row['website']=="") $mem_website="http://"; else $mem_website=$row['pdfurl']; ?>
<div class="form-group">
<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label"><b><?php print __("PDF url");?></b></label>
<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> >
<input type='text' class="form-control"  name='pdfurl' size='30' value='<?php print $mem_website; ?>' placeholder='http://' />
</div>
<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
</div>

<div class="form-group">
<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label"><b><?php print __("Visiting Card url");?></b></label>
<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> >
<input type='text' class="form-control"  name='vcardurl' size='30' value='<?php print $mem_website; ?>' placeholder='http://' />
</div>
<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
</div>

<p><b><?php print $relanguage_tags["Contact Information"];?>:</b></p>
<div class="form-group">
<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label" for="rename"><span class='required_field' ><sup>*<sup></span><b><?php print $relanguage_tags["Name"];?></b></label>
<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> ><input type='text' class="form-control" name='rename' id='rename' value='<?php print $row['name']; ?>' /></div>
<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
</div>

<div class="form-group" style='display:none;'>
<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label" for="rephone"><b><?php print $relanguage_tags["Phone"];?></b></label>
<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> ><input type='text' class="form-control"  name='rephone' id='rephone' value='<?php print $row['phone']; ?>' /></div>
<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
</div>

<div class="form-group">
<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label" for="reemail"><span class='required_field' ><sup>*<sup></span><b><?php print $relanguage_tags["Email"];?></b></label>
<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> ><input type='text' class="form-control"  name='reemail' id='reemail' value='<?php print $row['email']; ?>' /></div>
<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
</div>

<?php if($row['website']=="") $mem_website="http://"; else $mem_website=$row['website']; ?>
<div class="form-group">
    
<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label"><b><?php print $relanguage_tags["Website"];?></b></label>
<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> >
<input type='text' class="form-control"  name='rewebsite' size='30' value='<?php print $mem_website; ?>' />
<input type='hidden' name='ptype' value='myprofile' /><input type='hidden' name='formsubmit' value='1' />
</div>
<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
</div>

<div class="form-group" style='display:none;'>
<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label" for="remyaddress"><b><?php print $relanguage_tags["Address"];?></b></label>
<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> >
    <textarea class="form-control" name='remyaddress' cols='25' rows='4'><?php print $row['address']; ?></textarea>
    </div>
<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
</div>

<div class="form-group" style='display:none;'>
<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label"><b><?php print $relanguage_tags["Show profile image"];?>?</b></label>
<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> >
<label class="radio"><?php print $relanguage_tags["Yes"];?> <input type="radio"  name="reprofileimage" id='reprofileimage'  value="yes"></label>
<label class="radio"><?php print $relanguage_tags["No"];?> <input type="radio" name="reprofileimage" id='reprofileimage2' value="no"></label>
</div>
<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
</div>

<div id='listingProfileImage'></div>

<div class="form-group">
<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3"></div>  
<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> >
<input type='hidden' name='ptype' value='addReListing' />
<input type='submit' class='btn btn-primary btn-lg' id='reAddListingButton' value='<?php print $relanguage_tags["Add Listing"];?>' />
</div>
<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
<p class="help-block"><font style="font-size:10px; color:red;"><?php print $relanguage_tags["Fields marked with are required"];?></font></p>
</div>

</form>
</fieldset>
</div>
<?php 
return ob_get_clean();
} ?>