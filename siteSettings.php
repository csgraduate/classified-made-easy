<?php
/*
*     Author: Ravinder Mann
*     Email: ravi@codiator.com
*     Web: http://www.codiator.com
*     Release: 1.4
*
* Please direct bug reports,suggestions or feedback to :
* http://www.codiator.com/contact/
*
* Script: Classified made easy. Please respect the terms of your license. More information here: http://www.codecanyon.net/licenses
*
*/
function setOptionValues($host,$database,$username,$password,$adminOptionTable,$script="Classified Made Easy"){
	if(!isset($_SESSION["cladmin_settings"])) setReSession($host,$database,$username,$password,$adminOptionTable);
	return 	$_SESSION["cladmin_settings"];
}

function setReSession($host,$database,$username,$password,$adminOptionTable){
	$con=mysql_connect($host,$username,$password) or die("Could not connect. Please try again.");
	mysql_select_db($database,$con);
	mysql_query("SET NAMES utf8");
	$adminQr="select * from $adminOptionTable";
	$adminResult=mysql_query($adminQr);
	$adminOptions=mysql_fetch_assoc($adminResult);
	$_SESSION["cladmin_settings"]=$adminOptions;
		
	if(trim($adminOptions['siteoutercolor'])!="" || trim($adminOptions['siteheadercolor'])!="" || trim($adminOptions['siteheaderfontcolor'])!="" || trim($adminOptions['siteinnercolor'])!="" || trim($adminOptions['sitebordercolor'])!="" || trim($adminOptions['searchformcolor'])!="" || trim($adminOptions['searchformbordercolor'])!="" || trim($adminOptions['searchformfontcolor'])!="" || trim($adminOptions['menuboxcolor'])!="" || trim($adminOptions['menuboxfontcolor'])!="" || trim($adminOptions['menuboxbordercolor'])!=""){
		$_SESSION["recustom_settings"]=1;
		
	}
   
}

function setLanguageValues($host,$database,$username,$password,$languageTable,$redefaultLanguage){
	if(!isset($_SESSION["cl_language"])) setLanguageSession($host,$database,$username,$password,$languageTable,$redefaultLanguage);
	return 	$_SESSION["cl_language"];	
}

function setLanguageSession($host,$database,$username,$password,$languageTable,$redefaultLanguage){
	$con=mysql_connect($host,$username,$password) or die("Could not connect. Please try again.");
	mysql_select_db($database,$con);
	mysql_query("SET NAMES utf8");
	$redefaultLanguage=mysql_real_escape_string($redefaultLanguage);
	$adminQr="select * from $languageTable where language='$redefaultLanguage'";
	$adminResult=mysql_query($adminQr);
	while($langOptions=mysql_fetch_assoc($adminResult)){
		$languageTags[$langOptions['keyword']]=stripslashes($langOptions['translation']);
	}
	
	
	$_SESSION["cl_language"]=$languageTags;
	//mysql_close($con);
	 
}

function setAuth(){
$con=mysql_connect($GLOBALS['host'],$GLOBALS['username'],$GLOBALS['password']) or die("Could not connect. Please try again.");
    mysql_select_db($GLOBALS['database'],$con);
    mysql_query("SET NAMES utf8");
    $qr="select authorization_code, purchase_code from ".$GLOBALS['adminOptionTable'].";";
    $result=mysql_query($qr); 
    $row=mysql_fetch_assoc($result); 
    $_SESSION["cladmin_settings"]['authorization_code']=$row['authorization_code'];    
    $_SESSION["cladmin_settings"]['purchase_code']=$row['purchase_code'];
}

?>