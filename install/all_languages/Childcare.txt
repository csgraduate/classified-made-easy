﻿Arabic:
رعاية الأطفال
Bulgarian:
Грижите за децата
Catalan:
Cura de nens
Chinese Simplified:
托儿服务
Czech:
Péče o děti
Danish:
Børnepasning
Dutch:
Kinderopvang
English:
Childcare
Estonian:
Lastehoiuteenuste
Finnish:
Lastenhoito
French:
Garde d'enfants
German:
Kinderbetreuung
Greek:
Παιδική μέριμνα
Haitian Creole:
Gadri pou timoun
Hebrew:
ולהוקרה
Hindi:
चाइल्डकैअर
Hmong Daw:
Chaw
Hungarian:
Gyermekgondozás
Indonesian:
Pengasuhan anak
Italian:
Assistenza all'infanzia
Japanese:
保育
Korean:
육아
Latvian:
Bērnu aprūpe
Lithuanian:
Vaikų priežiūra
Norwegian:
Barnehage
Polish:
Opieka nad dziećmi
Portuguese:
Cuidados infantis
Romanian:
Îngrijire a copiilor
Russian:
Уход за детьми
Slovak:
Starostlivosť o deti
Slovenian:
Izvajalci
Spanish:
Cuidado de los niños
Swedish:
Konstnärer
Thai:
Childcare
Turkish:
Çocuk Bakımı
Ukrainian:
Догляду за дитиною
Vietnamese:
Chăm sóc trẻ em