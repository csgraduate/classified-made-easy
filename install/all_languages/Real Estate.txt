﻿Arabic:
العقارات
Bulgarian:
Недвижими имоти
Catalan:
Real Estate
Chinese Simplified:
房地产
Czech:
Reduced Homes for Sale
Danish:
Ejendomshandel
Dutch:
Onroerend goed
English:
Real Estate
Estonian:
Kinnisvara
Finnish:
Real Estate
French:
Immobilier
German:
Immobilien
Greek:
Real Estate
Haitian Creole:
Imobilye
Hebrew:
נדל ן
Hindi:
रियल एस्टेट
Hmong Daw:
Av
Hungarian:
Real Estate
Indonesian:
Real estat
Italian:
Immobiliare
Japanese:
不動産
Korean:
부동산
Latvian:
Nekustamais īpašums
Lithuanian:
Nekilnojamojo turto
Norwegian:
Eiendomsmegling
Polish:
Reduced Homes For Sale
Portuguese:
Imóveis
Romanian:
Real Estate
Russian:
Недвижимость
Slovak:
Real Estate
Slovenian:
Real Estate
Spanish:
Inmobiliaria
Swedish:
Fastigheter
Thai:
อสังหาริมทรัพย์
Turkish:
Dating
Ukrainian:
Нерухомість
Vietnamese:
Bất động sản