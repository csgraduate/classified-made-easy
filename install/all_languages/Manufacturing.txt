﻿Arabic:
الصناعة التحويلية
Bulgarian:
Производство
Catalan:
Fabricació
Chinese Simplified:
制造业
Czech:
Výroba
Danish:
Manufacturing
Dutch:
Productie
English:
Manufacturing
Estonian:
Töötlev tööstus
Finnish:
Valmistus
French:
Fabrication
German:
Fertigung
Greek:
Κατασκευής
Haitian Creole:
Fabrication
Hebrew:
ייצור
Hindi:
विनिर्माण
Hmong Daw:
Yeej
Hungarian:
Gyártási
Indonesian:
Manufaktur
Italian:
Di produzione
Japanese:
製造業
Korean:
제조
Latvian:
Ražošanas
Lithuanian:
Gamybos
Norwegian:
Produksjon
Polish:
Produkcja
Portuguese:
Fabricação
Romanian:
Fabricare
Russian:
Производство
Slovak:
Výroba
Slovenian:
Proizvodni
Spanish:
Fabricación
Swedish:
Tillverkning
Thai:
การผลิต
Turkish:
İmalat
Ukrainian:
Виробництво
Vietnamese:
Sản xuất