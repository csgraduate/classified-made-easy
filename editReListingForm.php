<?php 

$reqr1="select distinct category from listing where category<>''";
$resultre1=mysql_query($reqr1);

$reqr2="select distinct subcategory from listing where subcategory<>''";
$resultre2=mysql_query($reqr2);

$reqr3="select distinct activities from listing where activities<>''";
$resultre3=mysql_query($reqr3);

$reqr4="select distinct types from listing where types <>''";
$resultre4=mysql_query($reqr4);
?>
<div id='perimeter'>
<fieldset id='submitListingPage'>
<legend> 
<b><?php print $relanguage_tags["Edit Listing"];?></b>
</legend>
<form action='index.php' method='post' name='editReListingForm' enctype="multipart/form-data" class="form-horizontal">
<input type='hidden' id='isSubmitListingForm' name='isSubmitListingForm' value='1' />

<div class="form-group">
 <label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label" for="reCategory2"><span class='required_field'><sup>*<sup></span><b><?php print $relanguage_tags["Category"];?>:</b></label>
 <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> >
<select name='category' id='reCategory2' class="form-control">
<option value='Select' ><?php print $relanguage_tags["Select"]; ?></option>
<?php 
while($allCategories=mysql_fetch_assoc($resultre1)){ 
	?>
<option value='<?php print __($allCategories['category']);?>' <?php if($fullRelisting['category']==$allCategories['category']) print "selected='selected'"; ?> ><?php print __($allCategories['category']); ?></option>
<?php } 
?>
</select>
</div>
<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
</div>

<div id='subcategoriesSection2a'>
<div class="form-group">
 <label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label" for="reSubCategory2a"><span class='required_field' ><sup>*<sup></span><b><?php print __("Societies");?>:</b></label>
 <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> >
 <select name='subcategory' id='reCategorySubcategory2a' class="form-control">
 <option value='Select' selected='selected' ><?php print $relanguage_tags["Select"]; ?></option>
<?php 
while($allSubCategories=mysql_fetch_assoc($resultre2)){  ?>
<option  <?php if($fullRelisting['subcategory']==$allSubCategories['subcategory']) print "selected='selected'"; ?> value='<?php print __($allSubCategories['subcategory']);?>' ><?php print __($allSubCategories['subcategory']); ?></option>
<?php } ?>
 </select>
 </div>
 <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
</div>
    
    
</div>

<div id='activities'>
<div class="form-group">
 <label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label" for="activitiesSel"><span class='required_field' ><sup>*<sup></span><b><?php print __("Activities");?>:</b></label>
 <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> >
 <select name='activities' id='activitiesSel' class="form-control">
 <option  value='Select' selected='selected' ><?php print $relanguage_tags["Select"]; ?></option>
<?php 
while($record=mysql_fetch_assoc($resultre3)){  ?>
<option <?php if($fullRelisting['activities']==$record['activities']) print "selected='selected'"; ?>  value='<?php print __($record['activities']);?>' ><?php print __($record['activities']); ?></option>
<?php } ?>
 </select>
 </div>
 <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
</div>
    
    
</div>

<div id='types'>
<div class="form-group">
 <label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label" for="reSubCategory2a"><span class='required_field' ><sup>*<sup></span><b><?php print __("Types");?>:</b></label>
 <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> >
 <select name='types' id='typesSel' class="form-control">
 <option value='Select' selected='selected' ><?php print $relanguage_tags["Select"]; ?></option>
<?php 
while($record=mysql_fetch_assoc($resultre4)){  ?>
<option  <?php if($fullRelisting['types']==$record['types']) print "selected='selected'"; ?>  value='<?php print __($record['types']);?>' ><?php print __($record['types']); ?></option>
<?php } ?>
 </select>
 </div>
 <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
</div>
    
    
</div>


<div class="form-group" style='display:none;'>
<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label"><span class='required_field'><sup>*<sup></span><b><?php print $relanguage_tags["Listing by"];?>:</b></label>
<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> >
<label class="radio"><?php print $relanguage_tags["Individual"];?><input type="radio" id='byIndividual' name="relistingby"  <?php if($fullRelisting['relistingby']=="owner") print " checked "; ?> value="owner" /></label>
<label class="radio"><?php print $relanguage_tags["Other"];?> <input type="radio" id='reagentOther' name="relistingby" <?php if($fullRelisting['relistingby']=="reagent") print " checked "; ?>  value="reagent" /></label>
</div>
<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
</div>

<div id='byOtherSection' <?php if($fullRelisting['relistingby']=="owner") print 'style="display:none;"'; ?>>
<div class="form-group">
 <label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label" for="byother"><span class='required_field'><sup>*<sup></span><b><?php print $relanguage_tags["Please specify"];?>:</b></label>
 <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"> <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> 
 <input type='text' name='byother' class="form-control" id='byother' value='<?php print $fullRelisting['listing_by_other']; ?>' size='25' />
 </div>
 <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
 </div>
</div>

<?php if($memtype==9){ ?>
<div class="form-group">
<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label"><span class='required_field'><sup>*<sup></span><b><?php print $relanguage_tags["Listing status"];?>:</b> <font style="font-size:10px;">(<?php print $relanguage_tags["this option is visible to admin only"];?>)</font></label>
<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> >
<label class="radio"><?php print $relanguage_tags["Normal"];?> <input type="radio" name="listingexpire" id='listingNormal' <?php if($fullRelisting['listing_expire']=="normal") print "checked"; ?> value="normal" /></label>
<label class="radio"><?php print $relanguage_tags["Permanent"];?> <input type="radio" name="listingexpire" id="listingPermanent"<?php if($fullRelisting['listing_expire']=="permanent") print "checked"; ?>  value="permanent" /></label>
<span id='listingStatus'></span>
</div>
<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
</div>
<?php } ?>

<?php if($showPriceField=="true"){ ?>
<div class="form-group">
<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label" for="reprice"><b><?php print $relanguage_tags["Price"];?> (<?php print $defaultCurrency; ?>):</b></label>
<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> >
<input type='text' class='form-control textinput' name='reprice' id='reprice' size='7' value='<?php print $fullRelisting['price']; ?>' onkeyup="if(this.value.match(/[^0-9\. ]/g)) { this.value = this.value.replace(/[^0-9\. ]/g, '');}" />
</div>
<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
</div>
<?php  } ?>

<div id='priceField' >
<div class="form-group">
<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label" for="readdress"><b><?php print $relanguage_tags["Address"];?>:</b></label>
<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> >
<input type='text' class='form-control textinput' name='readdress' id='readdress' size='75' value="<?php print $fullRelisting['address']; ?>" />
<input type='hidden' name='readdress2' id='readdress2' size='75' value="<?php print $fullRelisting['address']; ?>" />
</div>
<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
</div>
</div>

<div class="form-group">
<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label" for="recity"><span class='required_field'><sup>*<sup></span><b><?php print $relanguage_tags["City"];?>:</b></label>
<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> >
<input type='text' class='form-control textinput' name='recity' id='recity' value="<?php print $fullRelisting['city']; ?>">
<input type='hidden' name='recity2'  id='recity2' value="<?php print $fullRelisting['city']; ?>">
</div>
<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
</div>

<div class="form-group">
<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label" for="restate"><b><?php print $relanguage_tags["State"];?>:</b></label>
<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> >
<input type='text' class='form-control textinput' name='restate'  id='restate' value="<?php print $fullRelisting['state']; ?>">
<input type='hidden' name='restate2'  id='restate2' value="<?php print $fullRelisting['state']; ?>">
</div>
<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
</div>

<div class="form-group">
<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label" for="recountry"><b><?php print $relanguage_tags["Country"];?>:</b></label>
<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> >
<input type='text' class='form-control textinput' name='recountry'  id='recountry' value="<?php print $fullRelisting['country']; ?>">
<input type='hidden' name='recountry2'  id='recountry2' value="<?php print $fullRelisting['country']; ?>">
</div>
<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
</div>

<div class="form-group">
<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label" for="repostal"><font face='verdana' size='1' color='red'></font><b><?php print $relanguage_tags["Postal Code"];?>:</b></label>
<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> >
<input type='text' class='form-control textinput' name='repostal' id='repostal' value='<?php print $fullRelisting['postal']; ?>'>
<input type='hidden' name='repostal2' id='repostal' value='<?php print $fullRelisting['postal']; ?>'>
</div>
<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
</div>


<div class="form-group">
<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label" for="customLocation"><b><?php print $relanguage_tags["Select custom location"];?>:</b></label>
<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> >
<input type='checkbox' id='customLocation'  />
<p class="help-block"><font style="font-size:10px;">(<?php print  __("You will be able to choose the address by clicking on map");?>)</font>
</div>
<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
</div>

<div id='listinglatLong' style="display:none;">
<div class="form-group">
<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label" for="listingLatitude"><b><?php print $relanguage_tags["Latitude"];?>:</b></label>
<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> ><input type='text' class="form-control" readonly name='latitude' value='<?php print $fullRelisting['latitude']; ?>' id='listingLatitude' /></div>
<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
</div>

<div class="form-group">
<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label" for="listingLongitude"><b><?php print $relanguage_tags["Longitude"];?>:</b></label>
<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> ><input type='text' class="form-control" readonly name='longitude' value='<?php print $fullRelisting['longitude']; ?>' id='listingLongitude' /></div>
<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
</div>
</div>

<div id='addListingMap' style="width:100%; height:400px; display:none;"></div>
<br /><br />


<div class="form-group">
<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label" for="reheadline"><span class='required_field'><sup>*<sup></span><b><?php print $relanguage_tags["Headline"];?>:</b></label>
<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
<input type='text' maxlength="150" class='form-control textinput' name='reheadline' id='reheadline' style="width:90%;" value="<?php print $fullRelisting['headline']; ?>">
</div>
</div>

<div class="form-group">
<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label" for="redescription"><span class='required_field'><sup>*<sup></span><b><?php print $relanguage_tags["Description"];?>:</b></label>
<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
<TEXTAREA NAME="redescription" id='redescription' class='form-control'  style="width:90%;" ROWS=25><?php print $fullRelisting['description']; ?></TEXTAREA>
</div>
</div>

<div class="form-group">
<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label"><b><?php print __("PDF url");?></b></label>
<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> >
<input type='text' class="form-control"  name='pdfurl' size='30' value='<?php print $fullRelisting['pdfurl']; ?>' placeholder='http://' />
</div>
<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
</div>

<div class="form-group">
<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label"><b><?php print __("Visiting Card url");?></b></label>
<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> >
<input type='text' class="form-control"  name='vcardurl' size='30' value='<?php print $fullRelisting['vcardurl']; ?>' placeholder='http://' />
</div>
<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
</div>

<p><b><?php print $relanguage_tags["Contact Information"];?>:</b></p>

<div class="form-group">
<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label" for="rename"><span class='required_field'><sup>*<sup></span><b><?php print $relanguage_tags["Name"];?></b></label>
<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> ><input type='text' class="form-control" name='rename' id='rename' value='<?php print $fullRelisting['contact_name']; ?>' /></div>
<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
</div>

<div class="form-group" style='display:none;'>
<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label" for="rephone"><b><?php print $relanguage_tags["Phone"];?></b></label>
<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> ><input type='text' class="form-control" name='rephone' id='rephone' value='<?php print $fullRelisting['contact_phone']; ?>' /></div>
<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
</div>

<div class="form-group">
<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label" for="reemail"><span class='required_field'><sup>*<sup></span><b><?php print $relanguage_tags["Email"];?></b></label>
<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> ><input type='text' class="form-control" name='reemail' id='reemail' value='<?php print $fullRelisting['contact_email']; ?>' /></div>
<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
</div>

<div class="form-group"><?php if($fullRelisting['contact_website']=="")$fullRelisting['contact_website']="http://"; ?>

<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label" for="rewebsite"><b><?php print $relanguage_tags["Website"];?></b></label>
<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> ><input type='text' class="form-control" name='rewebsite' id='rewebsite' size='30' value='<?php print $fullRelisting['contact_website']; ?>' />
<input type='hidden' name='ptype' value='myprofile' /><input type='hidden' name='formsubmit' value='1' /></div>
<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
</div>

<div class="form-group" style='display:none;'>
<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label" for="remyaddress"><b><?php print $relanguage_tags["Address"];?></b></label>
<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> ><textarea name='remyaddress' class="form-control" id='remyaddress' style="width:90%;" cols='25' rows='4'><?php print $fullRelisting['contact_address']; ?></textarea></div>
<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
</div>

<div class="form-group" style='display:none;'>
<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label"><b><?php print $relanguage_tags["Show profile image"];?>?</b></label>
<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> >
<label class="radio"><?php print $relanguage_tags["Yes"];?> <input type="radio"  <?php if($fullRelisting['show_image']=="yes") print " checked "; ?>  name="reprofileimage" id='reprofileimage'  value="yes"></label>
<label class="radio"><?php print $relanguage_tags["No"];?> <input type="radio" name="reprofileimage" id='reprofileimage2'  <?php if($fullRelisting['show_image']=="no") print " checked "; ?>   value="no"></label>
</div>
<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
</div>

<div id='listingProfileImage'></div>

<div class="form-group">
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3"></div>
<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:right;' "; ?> >
<input type='hidden' name='ptype' value='updateReListing' />
<input type='hidden' name='listingtype' value='<?php print $fullRelisting['listing_type']; ?>' />
<input type='hidden' name='reid' value='<?php print $fullRelisting['id']; ?>' />
<input type='submit' class='btn btn-primary btn-lg' id='reAddListingButton' value='<?php print $relanguage_tags["Update Listing"];?>' />
</div>
<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
<p class="help-block"><font style="font-size:10px; color:red;"><?php print $relanguage_tags["Fields marked with are required"];?></font></p>
</div>

</form>
</fieldset>
</div>