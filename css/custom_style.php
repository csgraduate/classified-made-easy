<?php
session_start();
header("Content-type: text/css");
$cladmin_settings=$_SESSION["cladmin_settings"];
//error_reporting(E_ALL ^ (E_NOTICE | E_WARNING | E_DEPRECATED));
error_reporting(0);
?>
body{font-size:13px;}
<?php
if($cladmin_settings['siteoutercolor']!=""){
?>
body{
background-color:<?php print $cladmin_settings['siteoutercolor']; ?>;
}
<?php } ?>

<?php if($cladmin_settings['webtheme']=="readable"){ ?>
body {
    padding-top: 0px !important;
}
<?php } ?>

<?php if($cladmin_settings['fixedtopheader']!="yes"){ ?>
	body{ padding-top: 0; }
<?php } ?>
#header{
<?php 
if($cladmin_settings['siteheadercolor']!="") print "background-color:". $cladmin_settings['siteheadercolor'].";";
if($cladmin_settings['siteheaderfontcolor']!="") print "color:". $cladmin_settings['siteheaderfontcolor'].";";
?>
}

#wrapper .container{
<?php 
if($cladmin_settings['siteinnercolor']!="") print "background-color:". $cladmin_settings['siteinnercolor'].";";
if($cladmin_settings['sitebordercolor']!="") print "border-color:". $cladmin_settings['sitebordercolor'].";";
?>
}

#sidebar1{
<?php 
if($cladmin_settings['searchformcolor']!="") print "background-color:". $cladmin_settings['searchformcolor'].";";
if($cladmin_settings['searchformbordercolor']!="") print "border-color:". $cladmin_settings['searchformbordercolor'].";";
if($cladmin_settings['searchformfontcolor']!="") print "color:". $cladmin_settings['searchformfontcolor'].";";
if(($_GET['ptype']=="showOnMap" && $_GET['fullscreen']=="true") || $_GET['fs']=="true"){ 
/*	print "position: fixed; ";
	print "padding:10px; ";
	print "z-index: 100; ";
	print "width:250px;
	left:0px;
	top:10px; "; */
}
?>
}

.fblogin li > a:hover{
background-color:#fff;
}

<?php if(($_GET['ptype']=="showOnMap" && $_GET['fullscreen']=="true") || $_GET['fs']=="true"){   ?>
#header{
display:none;
}

#sidebar{
position: absolute;
z-index: 100;
top:0px;
<?php if(isset($_SESSION["rtl"]) && !$_SESSION["rtl"]){ ?>left:0px; <?php } ?>
width:275px;
}

#footer{
display:none;
}

#mainContent{
z-index:11;
background-color:#fff;
}
<?php } ?>

<?php if($_SESSION["cladmin_settings"]["defaultlanguage"]=="Arabic" || $_SESSION["cladmin_settings"]["defaultlanguage"]=="Hebrew"){ ?> 
#sidebar{
right:0px;
}
<?php  } ?>

<?php if($_SESSION["cladmin_settings"]["defaultlanguage"]=="Arabic" || $_SESSION["cladmin_settings"]["defaultlanguage"]=="Hebrew"){?>
#logo{
float:right;
}
<?php } ?>

.smallOptional, .smallOptional a{
<?php 
if($cladmin_settings['searchformfontcolor']!="") print "color:". $cladmin_settings['searchformfontcolor'].";";
?>
}

#sidebarLogin{
<?php 
if($cladmin_settings['menuboxcolor']!="") print "background-color:". $cladmin_settings['menuboxcolor'].";";
if($cladmin_settings['menuboxbordercolor']!="") print "border-color:". $cladmin_settings['menuboxbordercolor'].";";
if($cladmin_settings['menuboxfontcolor']!="") print "color:". $cladmin_settings['menuboxfontcolor'].";";
?>
}

#footer,#footer p,#footer a{
<?php 
if($cladmin_settings['sitefooterfontcolor']!="") print "color:". $cladmin_settings['sitefooterfontcolor'].";";
?>
}

<?php if(trim($cladmin_settings['topmenubordercolor'])!=""){ ?>
#header_top_menu{
border:1px solid <?php print $cladmin_settings['topmenubordercolor']; ?>;
}

#header_top_menu ul li.first_item{
border-right:1px solid <?php print $cladmin_settings['topmenubordercolor']; ?>;
}
<?php  } ?>

<?php if(trim($cladmin_settings['topmenubackgroundcolor'])!=""){ ?>
#header_top_menu ul{
background-color: <?php print $cladmin_settings['topmenubackgroundcolor']; ?>;
}
<?php  } ?>

<?php if(trim($cladmin_settings['topmenufontcolor'])!=""){ ?>
#header_top_menu a{
color: <?php print $cladmin_settings['topmenufontcolor']; ?>;
}
<?php  } ?>

.nav-collapse .nav{
margin-right:0;
z-index:200;
}

<?php if($_SESSION["rtl"]){ ?>
#addListingMap img, #reListingOnMap img { 
  max-width: none;
}
#addListingMap label, #reListingOnMap label { 
  width: auto; display:inline; 
} 
.ui-multiselect {
     text-align: right;
}
<?php } ?>

.login_link{
    margin-left:30px;
}

#loginlink a{
    color:#fff;
}

#theListing{
<?php if(isset($_SESSION["rtl"]) && !$_SESSION["rtl"]){ ?>right:0px; <?php } ?>
}

[class^="icon-"], [class*=" icon-"] {
   vertical-align: middle;
   background-image:none;
}

