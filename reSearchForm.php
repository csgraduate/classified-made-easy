<?php 
include_once 'functions.inc.php';

if($reCategory[0]=="") $reCategory=htmlspecialchars($_POST['category'], ENT_QUOTES, 'UTF-8');
if($reSubcategory[0]=="") $reSubcategory=htmlspecialchars($_POST['subcategories'], ENT_QUOTES, 'UTF-8');
$rePrice=htmlspecialchars($_POST['price'], ENT_QUOTES, 'UTF-8');
if($reQuery=="") $reQuery=htmlspecialchars(trim($_POST['requery']), ENT_QUOTES, 'UTF-8');
if($reCity=="" && $reCity!="any") $reCity=htmlspecialchars(trim($_POST['city']), ENT_QUOTES, 'UTF-8'); 

if($reCategory=="") $reCategory=explode(",",$_SESSION["reCategory"]);
if($reSubcategory=="") $reSubcategory=explode(",",$_SESSION["reSubcategory"]);
if($rePrice=="") $rePrice=explode(",",$_SESSION["rePrice"]);
if($reQuery=="") $reQuery=$_SESSION["reQuery"];
if($reCity=="") $reCity=$_SESSION["reCity"];
if($reCity=="any") $reCity="";

$con=mysql_connect($host,$username,$password) or die("Could not connect. Please try again.");
mysql_select_db($database,$con);
mysql_query("SET NAMES utf8");
$reqr1="select distinct category from listing where category<>''";
$resultre1=mysql_query($reqr1);

$reqr2="select distinct subcategory from listing where subcategory<>''";
$resultre2=mysql_query($reqr2);

$reqr3="select distinct activities from listing where activities<>''";
$resultre3=mysql_query($reqr3);

$reqr4="select distinct types from listing where types <>''";
$resultre4=mysql_query($reqr4);

?>
<form id='reForm' method='post' name='form2' action='index.php' enctype="multipart/form-data" style='width:225px;'>
<div class="form-group">
<select name='category[]' multiple id='reCategory' >
<option value='<?php print $relanguage_tags["Any"]; ?>' <?php if(in_array($relanguage_tags["Any"],$reCategory) || $reCategory[0]=="" || $reCategory[0]=="Any") print "selected='selected'"; ?> ><?php print $relanguage_tags["Any"]; ?></option>
<?php 
while($allCategories=mysql_fetch_assoc($resultre1)){ ?>
<option value='<?php print __($allCategories['category']);?>' <?php if(in_array(__($allCategories['category']),$reCategory)) print "selected='selected'"; ?> ><?php print __($allCategories['category']); ?></option>
<?php } ?>
</select>
</div>

<div class="form-group">
<select name='subcategories[]' multiple id='reSubCategory' >
  <option value='<?php print $relanguage_tags["Any"]; ?>' <?php if(in_array($relanguage_tags["Any"],$reSubcategory) || $reSubcategory[0]=="") print "selected='selected'"; ?> ><?php print $relanguage_tags["Any"]; ?></option>
<?php 
while($records=mysql_fetch_assoc($resultre2)){ ?>
<option value='<?php print __($records['subcategory']);?>' <?php if(in_array(__($records['subcategory']),$reSubcategory)) print "selected='selected'"; ?> ><?php print __($records['subcategory']); ?></option>
<?php } ?>
</select>
</div>

<div class="form-group">
<select name='activities[]' multiple id='reActivities' >
<option value='<?php print $relanguage_tags["Any"]; ?>' <?php if(in_array($relanguage_tags["Any"],$reActivities) || $reActivities[0]=="" || $reActivities[0]=="Any") print "selected='selected'"; ?> ><?php print $relanguage_tags["Any"]; ?></option>
<?php 
while($records=mysql_fetch_assoc($resultre3)){ ?>
<option value='<?php print __($records['activities']);?>' <?php if(in_array(__($records['activities']),$reActivities)) print "selected='selected'"; ?> ><?php print __($records['activities']); ?></option>
<?php } ?>
</select>
</div>

<div class="form-group">
<select name='types[]' multiple id='reTypes' >
<option value='<?php print $relanguage_tags["Any"]; ?>' <?php if(in_array($relanguage_tags["Any"],$reTypes) || $reTypes[0]=="" || $reTypes[0]=="Any") print "selected='selected'"; ?> ><?php print $relanguage_tags["Any"]; ?></option>
<?php 
while($records=mysql_fetch_assoc($resultre4)){ ?>
<option value='<?php print __($records['types']);?>' <?php if(in_array(__($records['types']),$reTypes)) print "selected='selected'"; ?> ><?php print __($records['types']); ?></option>
<?php } ?>
</select>
</div>


<div style='display:none;' id='rePriceRange' <?php if($showPrice) print " style='display:block;' "; else print " style='display:none;' "; ?>>
<div class="form-group">
<select name="price[]" multiple id="rePrice">
<option value="10"  <?php if(in_array(10,$rePrice) || $rePrice[0]=="") print "selected='selected'"; ?> ><?php print $relanguage_tags["Any Range"];?></option>
<?php for($i=0;$i<$rentRangeSize;$i++){
list($opriceFrom,$opriceTo)=explode("-",$rentPriceRange[$i]);  
if($opriceTo=="Above") $opricerange=$opriceFrom."-".__("Above");   
else $opricerange=$rentPriceRange[$i];

list($priceFrom,$priceTo)=explode("-",$rentPriceRange[$i]);

if(trim($priceFrom)!="" && $priceTo!=""){
    if($priceTo!="Above"){
      if($currency_before_price) $priceTo=$defaultCurrency.$priceTo;
      else $priceTo=$priceTo." ".$defaultCurrency;
     }    
    if($priceTo=="Above") $priceToTrans=__($priceTo); else $priceToTrans=$priceTo;
    if($currency_before_price) $priceFrom=$defaultCurrency.$priceFrom;
    else $priceFrom=$priceFrom." ".$defaultCurrency; 
    
?>
<option value="<?php print str_replace("Above", __("Above"), $rentPriceRange[$i]); ?>" <?php if(in_array($opricerange,$rePrice)) print "selected='selected'"; ?> ><?php print $priceFrom." - ".$priceToTrans; ?></option>
<?php  } } ?>

</select> 
</div>
</div>

<div class="form-group">
<input size='32' style='width:225px;' type='text' class="form-control"  name='requery' value='<?php print htmlspecialchars($reQuery, ENT_QUOTES, 'UTF-8'); ?>' id='reQuery' placeholder='<?php print $relanguage_tags["Keyword"];?>/<?php print $relanguage_tags["Street"];?>/<?php print $relanguage_tags["ID"];?>/<?php print $relanguage_tags["Postal"];?>'>
</div>

<div class="form-group">
<input size='32' style='width:225px;' type='text' class="form-control" name='city' value='<?php print htmlspecialchars($reCity, ENT_QUOTES, 'UTF-8'); ?>' id='reCity' placeholder='<?php print $relanguage_tags["City"];?>'>
<input type='hidden' name='ptype' id='sfpType' value='showOnMap'>
</div>


<div class="form-group" style="text-align:center;">
<?php if($fullScreenEnabled!="true"){ ?>
<button type='button' class='rebutton btn btn-sm btn-primary' id='reSearch' ><i class="icon-search"></i> <?php print $relanguage_tags["Search"];?></button>&nbsp;&nbsp;&nbsp;&nbsp;
<?php  } ?>
<?php if(($ptype=="showOnMap" && $_GET['fullscreen']=="true") || $fullScreenEnabled=="true"){ ?>
<button type='button'  class='rebutton  btn btn-sm btn-primary'  id='reSearchMap2'><i class="icon-map-marker"></i> <?php print $relanguage_tags["Show on map"]; ?></button>
<?php }else{ ?>
<button type='submit'  class='rebutton  btn btn-sm btn-primary'  id='reSearchMap2'><i class="icon-map-marker"></i> <?php print $relanguage_tags["Show on map"]; ?></button>
<?php } ?>

</div>
<div id='modeButton'><a class='rebutton  btn btn-danger' style='background-color:#fff; border:1px #fff solid; color:#d2d2d2;'><i class="icon-circle"></i> <?php print __("Reset"); ?></a></div>
</form>