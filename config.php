<?php
/*
*     Author: Ravinder Mann
*     Email: ravi@codiator.com
*     Website: http://www.codiator.com
*     Release: 1.4
*
* Please direct bug reports,suggestions or feedback to :
* http://www.codiator.com/contact/
*
* Script: Classified made easy. Please respect the terms of your license. More information here: http://www.codecanyon.net/licenses
*  
*/
error_reporting(0);
session_start();

include("site.inc.php");
include_once("plugin_handler.inc.php"); 
include_once "plugins.php";
if($host=="" || $database=="" || $username=="") header('Location: install/index.php');

$reListingTable="listing";
$rememberTable="member";
$adminOptionTable="admin_options";
$languageTable="languages";
$priceTable="pricerange";
$categoryTable="categories";
$pageTable="pages";
$reMaxPictures=15;
$isThisDemo="no"; 
$version_num="Release: 1.4.9.1";
$changelog="https://www.finethemes.com/changelog/index.php?id=9&ver=";

$SMTPAuth=true;
$customMarkers=false;
/*
 * $defaultCityZoom This is the zoom level of the visitor city and it is used when visitor first visits the website and geoIP is enabled in the admin options. 
 * $defaultCountryZoom is the zoom level of the default country if there's not a single listing and map loads the default country set in admin options.
 * In all other cases the script automatically sets the zoom level as per the number of markers found in a given area.
 */
$defaultCityZoom=11;
$defaultCountryZoom=4;

include_once("siteSettings.php");
$cladmin_settings=setOptionValues($host,$database,$username,$password,$adminOptionTable);

if(trim($cladmin_settings["fullscreenenabled"]=="true")) $fullScreenEnabled="true"; else $fullScreenEnabled="false";
$enableRegisterCaptcha = $cladmin_settings["enableregistercaptcha"] ? true : false;
$_SESSION['rtl']=$rtl = $cladmin_settings["rtl"] ? true : false;
if(isset($_GET['rtl']) && $_GET['rtl']==1)$_SESSION['rtl']=$rtl=true;
$google_login = $cladmin_settings["google_login"] ? true : false;
$yahoo_login = $cladmin_settings["yahoo_login"] ? true : false;
$currency_before_price = $cladmin_settings["currency_before_price"] ? true : false;
 
$webtheme=trim($cladmin_settings['webtheme']);
if(isset($_GET['theme'])) $_SESSION['theme']=$_GET['theme'];
if(isset($_SESSION['theme']))$webtheme=trim($_SESSION['theme']);
$fieldtheme=trim($cladmin_settings['fieldtheme']);
$fixedtopheader=$cladmin_settings['fixedtopheader'];
$notifyadmin=$cladmin_settings['notifyadmin'];
$listingreview=$cladmin_settings['listingreview'];
$listingemail=$cladmin_settings['listingemail'];
$defaultCurrency=trim($cladmin_settings['defaultcurrency']);
$redefaultCountry=trim($cladmin_settings['defaultcountry']);
$defaultcountry_latlng=trim($cladmin_settings['defaultcountry_latlng']);
$redefaultLanguage=trim($cladmin_settings['defaultlanguage']);
$refriendlyurl=trim($cladmin_settings['refriendlyurl']);
$emaildebug=trim($cladmin_settings['emaildebug']);
if($redefaultLanguage=="")$redefaultLanguage="English";
if(!isset($_SESSION["cl_language"]) || !isset($_SESSION["cl_custom_lang"])){
	$_SESSION["cl_custom_lang"]=$redefaultLanguage;
	$relanguage_tags=setLanguageValues($host,$database,$username,$password,$languageTable,$redefaultLanguage);
}
else $relanguage_tags=$_SESSION["cl_language"];
$ppcurrency=trim($cladmin_settings['ppcurrency']);
$ppemail=trim($cladmin_settings['ppemail']);
$featuredduration=trim($cladmin_settings['featuredduration']);
$featuredprice=trim($cladmin_settings['featuredprice']);
$resmtp=trim($cladmin_settings['resmtp']);
$resmtpport=trim($cladmin_settings['resmtpport']);
$gmailUsername=trim($cladmin_settings['smtpusername']);
$gmailPassword=trim($cladmin_settings['smtppassword']);
$reSiteName=trim($cladmin_settings['websitetitle']);
$reSiteFooter=trim($cladmin_settings['websitefooter']);
$geoipenable=trim($cladmin_settings['geoipenable']);
$WordPressAPIKey = trim($cladmin_settings['wordpressapikey']);
$reCaptchaPrivateKey = trim($cladmin_settings['recaptchaprivatekey']);
$reCaptchaPublicKey =trim($cladmin_settings['recaptchapublickey']);
$googleMapAPIKey=trim($cladmin_settings['googlemapapikey']);
$browsertitle=trim($cladmin_settings['browsertitle']);
$homepagedescription=trim($cladmin_settings['homepagedescription']);
$homepagekeywords=trim($cladmin_settings['homepagekeywords']);
$toplinkad=trim($cladmin_settings['toplinkad']);
$sidebarad=trim($cladmin_settings['sidebarad']);
$contactaddress=trim($cladmin_settings['contactaddress']);
$contactformemail=trim($cladmin_settings['contactformemail']);
$headlinelength=trim($cladmin_settings['headlinelength']);
$descriptionlength=trim($cladmin_settings['descriptionlength']);
$delete_after_days=trim($cladmin_settings['delete_after_days']);
$fb_app_id=trim($cladmin_settings['fb_app_id']);
$fb_app_secret=trim($cladmin_settings['fb_app_secret']);
$gclientid=trim($cladmin_settings['gclientid']);
$gclientsecret=trim($cladmin_settings['gclientsecret']);

if(isset($_SESSION["cladmin_settings"]['authorization_code']) && isset($_SESSION["cladmin_settings"]['purchase_code'])){
$authorization_code=trim($_SESSION["cladmin_settings"]['authorization_code']);
$purchase_code=trim($_SESSION["cladmin_settings"]['purchase_code']);
}else{
setAuth();
$authorization_code=trim($_SESSION["cladmin_settings"]['authorization_code']);
$purchase_code=trim($_SESSION["cladmin_settings"]['purchase_code']);    
}

?>