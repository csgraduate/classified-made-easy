<?php
/*
*     Author: Ravinder Mann
*     Email: ravi@codiator.com
*     Web: http://www.codiator.com
*     Release: 1.4
*
* Please direct bug reports,suggestions or feedback to :
* http://www.codiator.com/contact/
* 
* Script: Classified made easy. Please respect the terms of your license. More information here: http://www.codecanyon.net/licenses
*
*/
//error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
error_reporting(0);

//if(function_exists('date_default_timezone_set')) date_default_timezone_set('America/Toronto');
$oodleListings=array();

/*  Filters the output of searchResults2 through registerd hooks */
function searchResults($reFullQuery,$onlyMemberListings="no",$showFavorite="no"){
 /*
 * Filters data through the functions registered for "searchResults" hook.
 * Passes the content through registered functions.
 */
    print call_plugin("searchResults",searchResults2($reFullQuery,$onlyMemberListings,$showFavorite));
}

/*  This function gets the search criteria and shows listings as per it */
function searchResults2($reFullQuery,$onlyMemberListings="no",$showFavorite="no"){
include("config.php");
ob_start(); 
list($reCategory,$reSubcategory,$rePrice,$reQuery,$reCity,$listingsPerPage,$pageNum,$sortby)=explode(":",$reFullQuery);
$rePartialQuery="$reCategory:$reSubcategory:$rePrice:$reQuery:$reCity";
$mem_id=$_SESSION["re_mem_id"];
reSetSearchSession($reCategory,$reSubcategory,$rePrice,$reQuery,$reCity);
//print "$reFullQuery<br />";
//print $relanguage_tags[];
if($onlyMemberListings=="yes"){
	$extraMessage=" your ";
	$functionType=7;
}else{
	$functionType=1;
}
if($showFavorite==="yes") $functionType=24;

if($pageNum=="")$pageNum=1;

if($listingsPerPage=="")$listingsPerPage=20;
$startListingNum=(($pageNum-1)*$listingsPerPage);

if($startListingNum<0) $startListingNum=0;
$endListingNum=$startListingNum+$listingsPerPage;
$startListingNum2=$startListingNum+1;

if($sortby==""){
	$sortby="dateUp";
}

if($sortby=="priceUp"){
	$priceSort="priceDown";
	$priceSortClass=$priceSort;
	$sortbyClause=" price desc ";
}
if($sortby=="priceDown"){
	$priceSort="priceUp";
	$priceSortClass=$priceSort;
	$sortbyClause=" price asc ";
}
if($priceSort==""){
$priceSort="priceDown";
$priceSortClass="";
}
if($sortby=="cityUp"){
	$citySort="cityDown";
	$citySortClass=$citySort;
	$sortbyClause=" city desc ";
}
if($sortby=="cityDown"){
	$citySort="cityUp";
	$citySortClass=$citySort;
	$sortbyClause=" city asc ";
}
if($citySort==""){
	$citySort="cityDown";
	$citySortClass="";
}
if($sortby=="dateUp"){
	$dateSort="dateDown";
	$dateSortClass=$dateSort;
	$sortbyClause=" dttm desc ";
}
if($sortby=="dateDown"){
	$dateSort="dateUp";
	$sortbyClause=" dttm asc ";
	$dateSortClass=$dateSort;
}
if($dateSort==""){
	$dateSort="dateDown";
	$dateSortClass="";
}

if($delete_after_days>0){
$str_older = date("Y-m-d");
$str_month = date("n");
$str_day = date("j");
$str_year = date("Y");
$minmonth = mktime(0, 0, 0, $str_month, $str_day - $delete_after_days ,  $str_year );
$str_older = date("Y-m-d",$minmonth);
$str_older_dttm="$str_older 23:59:59";

$qr00="select count(*) from $reListingTable";
$result00=mysql_query($qr00);
$row00=mysql_fetch_array($result00);
$numResults00=$row00[0];
$dttm_mod_clause=" and dttm_modified > '$str_older_dttm' ";
}else{
$dttm_mod_clause="";	
}

if(isset($_SESSION["memtype"]) && $_SESSION["memtype"]==9) $adminClause1=" id like '%' ";
else  $adminClause1=" listing_type <> 3 ";

if($functionType==7) $adminClause1=" id like '%' ";
if(isset($_SESSION["memtype"]) && $_SESSION["memtype"]==9){
$flagClause=" flag desc, ";    
}else $flagClause="";

if($showFavorite=="no"){
if($cladmin_settings['oodleplugin']==1 && function_exists("getOodleArray") && $onlyMemberListings=="no"){
$qr0="select * from $reListingTable where $adminClause1 ".getRealValue($reCategory,"reCategory").getRealValue($reSubcategory,"reSubcategory")
.getRealValue($rePrice,"rePrice").getRealValue($onlyMemberListings,"onlyMemberListings").getRealValue($reQuery,"reQuery").getRealValue($reCity,"reCity")." 
 $dttm_mod_clause and listing_type<>'2' ; ";
$result0=mysql_query($qr0);
$numResults=mysql_num_rows($result0);

$qr1="select * from $reListingTable where $adminClause1 ".getRealValue($reCategory,"reCategory").getRealValue($reSubcategory,"reSubcategory")
.getRealValue($rePrice,"rePrice").getRealValue($onlyMemberListings,"onlyMemberListings").getRealValue($reQuery,"reQuery").getRealValue($reCity,"reCity")
." $dttm_mod_clause and listing_type<>'2' order by $flagClause listing_type DESC, $sortbyClause ; ";
$result=mysql_query($qr1);

$qr2="select * from $reListingTable where $adminClause1 ".getRealValue($reCategory,"reCategory").getRealValue($reSubcategory,"reSubcategory")
.getRealValue($rePrice,"rePrice").getRealValue($onlyMemberListings,"onlyMemberListings").getRealValue($reQuery,"reQuery").getRealValue($reCity,"reCity")
." $dttm_mod_clause and  listing_type='2' order by $sortbyClause ; ";
$result2=mysql_query($qr2);
$featuredListings=array();
while($line = mysql_fetch_assoc($result2)) $featuredListings[] = $line;

$cListings=array();
$oodleListingArray=array();

require_once('geoplugin.class.php');
$geoplugin = new geoPlugin();
$geoplugin->locate();
$vCountry=$geoplugin->countryName;
$oodlecRegion=getOodleRegion($vCountry);

if($oodlecRegion!="no"){
	if($listingOffset==0 || $listingOffset=="") $listingOffset=1;
	while($line = mysql_fetch_assoc($result)) $cListings[] = $line;
	list($price1,$price2)=explode("-", $rePrice);
	if($rePrice!=10) $attr="price_".$price1."_".$price2;
	$oodleSubcats=explode(",",$reSubcategory);
	foreach($oodleSubcats as $oindex => $osubcatkey){
		$oodleListingArray=array_merge($oodleListingArray,getOodleArray($reCategory,$osubcatkey,$reQuery,$reCity,$oodlecRegion,$reMaxPictures,1,50,$attr));
	}
}

if(is_array($oodleListingArray)) $combArray=array_merge($cListings,convertArrayToClFormat($oodleListingArray));
else $combArray=$cListings;

if($sortby==="")$sortby="dateUp";
usort($combArray, $sortby);
$combArray=array_merge($featuredListings,$combArray);
$totalCombLists=sizeof($combArray);
$numResults=$totalCombLists;
$startFrom=$startListingNum;
$endAt=$endListingNum;
if($endAt>$numResults) $endAt=$numResults;
}else{
	$qr0="select * from $reListingTable where $adminClause1 ".getRealValue($reCategory,"reCategory").getRealValue($reSubcategory,"reSubcategory")
	.getRealValue($rePrice,"rePrice").getRealValue($onlyMemberListings,"onlyMemberListings").getRealValue($reQuery,"reQuery").getRealValue($reCity,"reCity")."
	$dttm_mod_clause ; ";
	$result0=mysql_query($qr0);
	$numResults=mysql_num_rows($result0);
		
	$qr1="select * from $reListingTable where $adminClause1 ".getRealValue($reCategory,"reCategory").getRealValue($reSubcategory,"reSubcategory")
	.getRealValue($rePrice,"rePrice").getRealValue($onlyMemberListings,"onlyMemberListings").getRealValue($reQuery,"reQuery").getRealValue($reCity,"reCity")
	." $dttm_mod_clause order by $flagClause listing_type DESC, $sortbyClause limit $startListingNum,$listingsPerPage ; ";
	$result=mysql_query($qr1);
	while($line = mysql_fetch_assoc($result)) $combArray[] = $line;
	$startFrom=0;
	$endAt=sizeof($combArray); 
    
}

}else{
$allfavs=explode(":",$_SESSION["marked_reid"]);
$favcounter=0;
foreach($allfavs as $favc => $favid){
	$favArr=getFavRecord($favid);
	if(is_array($favArr)) $combArray[$favcounter]=$favArr; 
	$favcounter++;
}
if($sortby==="")$sortby="dateUp";
usort($combArray, $sortby);
$startFrom=0;
$endAt=sizeof($combArray);
$numResults=$endAt;
$favClause1=" ".__("Favorite")." ";
//print "size: $endAt"; print_r($combArray);
}

$totalPages=ceil($numResults/$listingsPerPage);
if($endListingNum>$numResults)$endListingNum=$numResults;

$combArray=call_plugin("searchResultsRecords",$combArray);

$qrp="select price from $categoryTable where id like '%' and price='true' ".getRealValue($reCategory,"reCategory");
$resultp=mysql_query($qrp);
if(mysql_num_rows($resultp)>0){
	$showPrice="true";
	$otherRowSpan=5;
	$cityAlignment=" ";
}
else{
	$showPrice="false";
	$otherRowSpan=4;
	$cityAlignment=" align='center' ";
}

if($numResults>0){
print "<table id='resultTable' class='table table-striped table-hover'>";
print "<thead><tr class='headRow1'><th colspan='$otherRowSpan'><div class='pull-left'>".$relanguage_tags["Showing"]." <b>$startListingNum2 - $endListingNum</b> ".__("of")." $extraMessage<b>$numResults</b> ".$favClause1.$relanguage_tags["Listings"].". 
</div>"; 
if($pageNum==1){
?>
<div class='pull-right'><?php print $relanguage_tags["Show"];?> <select name='subtype' id='reListingsPerPage1' >
<option value='<?php print "$rePartialQuery:20:$pageNum:$sortby-@@-$functionType"; ?>' <?php if($listingsPerPage==20)print " selected='selected' "; ?>>20</option>
<option value='<?php print "$rePartialQuery:40:$pageNum:$sortby-@@-$functionType"; ?>' <?php if($listingsPerPage==40)print " selected='selected' "; ?>>40</option>
<option value='<?php print "$rePartialQuery:60:$pageNum:$sortby-@@-$functionType"; ?>' <?php if($listingsPerPage==60)print " selected='selected' "; ?>>60</option>
<option value='<?php print "$rePartialQuery:80:$pageNum:$sortby-@@-$functionType"; ?>' <?php if($listingsPerPage==80)print " selected='selected' "; ?>>80</option>
<option value='<?php print "$rePartialQuery:100:$pageNum:$sortby-@@-$functionType"; ?>' <?php if($listingsPerPage==100)print " selected='selected' "; ?>>100</option>
<?php 
print "</select> ".$relanguage_tags["Listings"]."/".$relanguage_tags["page"];
}
print "</div></th></tr></thead>";
print "<tbody><tr class='headRow2'><td></td><td>".$relanguage_tags["Title"]."</td>"; 
if($showPrice=="true"){
?>
<td align='center' class='hsorting'><a href='#'><span style="display:none;"><?php print "$rePartialQuery:$listingsPerPage:$pageNum:$priceSort-@@-$functionType"; ?></span><?php print $relanguage_tags["Price"];?></a><div class="<?php print $priceSortClass; ?>"></div></td>
<?php } ?>
<td class='hsorting'><a href='#'><span style="display:none;"><?php print "$rePartialQuery:$listingsPerPage:$pageNum:$citySort-@@-$functionType"; ?></span><?php print $relanguage_tags["City"];?></a><div class='<?php print $citySortClass; ?>'></div></td>
<td class='hsorting'><a href='#'><span style="display:none;"><?php print "$rePartialQuery:$listingsPerPage:$pageNum:$dateSort-@@-$functionType"; ?></span><?php print $relanguage_tags["Date"];?></a><div class='<?php print $dateSortClass; ?>'></div></td></tr>
<?php 
$count=0;

for($c=$startFrom;$c<$endAt;$c++){
if($count>=$listingsPerPage) break;
$row=$combArray[$c]; 
$smallDesc=utf8_substr($row['description'],0,250)."....";
//$smallHeadline=substr($row['headline'],0,75);
list($listingDate,$listingTime)=explode(" ",$row['dttm']);
if($row['user_id']!="oodle") $rePicArray=explode("::",$row['pictures']);
else{
   // $oodleListingArray=convertArrayToClFormat(getOodleArray("","",$id,"",$region));
  //  $row=$oodleListingArray[0];
    $rePicArray=explode("::",$row['pictures']);
    $totalRePics=sizeof($rePicArray);
    if ($totalRePics > $reMaxPictures) $totalRePics = $reMaxPictures; 
}
if($row['user_id']!="oodle") $totalRePics=sizeof($rePicArray)-1; else $totalRePics=sizeof($rePicArray);
if($row['price']==0)$row['price']="";
if($row['resize']==0)$row['resize']="";

$row['price']=number_format($row['price'],2);

if($row['user_id']=="oodle") $defaultCurrency=getOodleCurrency($oodlecRegion,$defaultCurrency);
if($row['price']=="") $priceValue=""; 
else{
    if($currency_before_price) $priceValue=$defaultCurrency.$row['price'];
    else $priceValue=$row['price']." ".$defaultCurrency;
}

if($totalRePics>=1) $firstPic=$rePicArray[0];
else $firstPic="images/no-image.png";
if(trim($firstPic)==""){ $firstPic="images/no-image.png"; $totalRePics=0; }

if($row['listing_type']==2){ $featuredClass= " featuredClass alert-info "; $featuredClassChild=" speciallistingChild "; }
elseif($row['listing_type']==3){$featuredClass= " inavtiveClass alert-danger "; $featuredClassChild=" inactivelistingChild "; }
else{ $featuredClass=""; $featuredClassChild=""; }

if($row['flag']>0 && isset($_SESSION["memtype"]) && $_SESSION["memtype"]==9){
    $featuredClass= " flagClass alert-warning "; $featuredClassChild=" flaglistingChild ";
}
//$listingDate= date('d/m/y', strtotime($listingDate));

if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true){
    $listingImageClause=" pull-right ";
    $listingstatusClause=" style='float:right;' ";
    $listingLabelClause=" pull-left ";
    
}else{
    $listingImageClause=" pull-left ";
    $listingstatusClause=" style='float:left;' ";
    $listingLabelClause=" pull-right ";
}

print "<tr class='resultRow $featuredClass' onClick=\"toggleRow(".$row['id'].")\" id='parent-row".$row['id']."'>
<td class='colhide'><img  id='img".$row['id']."'src='images/plus.png' /></td>
<td class='resultHeading'><span class='headtext'>".$row['headline']."</span>
<br /><span class='$spanClass1' style='padding:0 2px; margin:0; font-size: 85%;'>".__($row['category'])." - ".__($row['subcategory'])." (#".$row['id'].")</span><div class='$listingLabelClause'>".hasVisited($row['id']);
if($row['listing_type']==2){ print "<span class='featuredlisting label label-primary'>".$relanguage_tags["Featured"]."</span>"; }
if($row['flag']>0 && isset($_SESSION["memtype"]) && $_SESSION["memtype"]==9){ print "<span class='featuredlisting label label-danger'>".__("Flagged")." ".$row['flag']." ".__("times")."</span>"; }
print "</div></td>";
if($showPrice=="true"){
print "<td>$priceValue</td>";
}
print "<td>".$row['city']."</td>
<td>".$listingDate."</td></tr>";

?>
<tr id='child-row<?php print $row['id']; ?>' style="display:none" class='resultChild <?php print $featuredClassChild; ?>'>
<td></td><td class='childImageCol' colspan='4'>
<div class='listingSmallImage <?php print $listingImageClause; ?>'>
<?php if($row['user_id']=="oodle") $oodleregion="&region=".$row['country']; else $oodleregion=""; ?>
<!--<a href='gallery.php?adid=<?php print $row['id'].$oodleregion; ?>' ><img width='100' border='0' src='timthumb.php?src=<?php print $firstPic; ?>' /></a>--> 
<?php if($firstPic!="images/no-image.png"){
$style_attr=" style='display:block;' ";    
foreach($rePicArray as $pid=>$pval){
if(trim($pval)!=""){        
?>    
<a rel='prettyPhoto[<?php print $row['id']; ?>]' <?php print $style_attr; ?> href='<?php print $pval; ?>' ><img border='0' src='timthumb.php?src=<?php print $pval; ?>&w=70' /></a>
<?php
$style_attr=" style='display:none;' ";
}  
}
}else{ ?>
<img border='0' src='timthumb.php?src=<?php print $firstPic; ?>&w=70' />    
<?php } ?>
<br /><span>
<?php 
if($totalRePics<2 && $totalRePics>0){
?>
<?php print $totalRePics." Picture"; ?>	
<?php }
if($totalRePics>1){ if($totalRePics>$reMaxPictures) $totalRePics=$reMaxPictures; ?>
<?php print $totalRePics." ".$relanguage_tags["Pictures"]; ?>
<?php } ?>

</span>

</div>
<div class='childSmallDesc'><?php print $smallDesc; ?>
<?php if($row['retype']==$relanguage_tags["Residential"]){ 
$bedArray=preg_split('#(?<=\d)(?=[a-z])#i', $row['bedrooms']);
	?>
<div class='listingItem'><ul><li><b><?php print $relanguage_tags["Bedroom"];?>:</b><span class='listingItemValue'><?php print $bedArray[0]." ".$relanguage_tags[$bedArray[1]]; ?></span></li>
<li><b><?php print $relanguage_tags["Bathroom"];?>:</b><span class='listingItemValue'><?php print $row['bathrooms']; ?></span></li>
<li><b><?php print $relanguage_tags["Size"];?>:</b><span class='listingItemValue'><?php print $row['resize']; ?> Sq.Ft</span></li>
</ul></div>
<?php } ?>

</div> 

<div class='row' style='clear:both; margin:0 15px;'>
<?php
$navGridSize=12;
if($_SESSION["memtype"]==9 && $row['user_id']!="oodle"){
	?>
<div class='listing_status col-md-4 col-lg-4' <?php print $listingstatusClause; ?> id='listingstatus<?php print $row['id']; ?>' ><?php print $relanguage_tags["Set listing type to"];?>: 
<select name='listingtype' id='listingtype' onchange="infoResults(this.value+'<?php print "::".$row['id']; ?>',14,'listingstatus<?php print $row['id']; ?>');">
<option value='1' <?php if($row['listing_type']=="" || $row['id']==1){ print "selected='selected'"; } ?>><?php print $relanguage_tags["Normal"];?></option>
<!-- <option value='3' <?php if($row['listing_type']==3){ print "selected='selected'"; } ?>>Top</option> -->
<option value='2' <?php if($row['listing_type']==2){ print "selected='selected'"; } ?>><?php print $relanguage_tags["Featured"];?></option>
<option value='3' <?php if($row['listing_type']==3){ print "selected='selected'"; } ?>><?php print __("Inactive");?></option>
</select>
</div>
<?php
$navGridSize=8; 	
}
$headline_slug=friendlyUrl($row['headline']);
if($row['user_id']=="oodle") $region_slug=$row['country']; else $region_slug="";
if($refriendlyurl=="enabled") $relistingLink=friendlyUrl($row['category'],"_")."/".friendlyUrl($row['subcategory'],"_")."/"."id-".$row['id']."-".$region_slug."-".$headline_slug;
else $relistingLink="index.php?ptype=viewFullListing&reid=".$row['id']."&title=$title_slug".$oodleregion;
?>
<div class='col-md-<?php print $navGridSize; ?> col-lg-<?php print $navGridSize; ?>'>
<?php showMemberNavigation($row['user_id'],$mem_id,$row['id'],1); ?>
<a class='moreInfo btn btn-sm btn-success' href='<?php print $relistingLink; ?>'><?php print $relanguage_tags["More Information"];?></a>
</div>
</div>

</td></tr>
<?php 	
$count++;
//}
}
if($delete_after_days>0){
if ($numResults00>$numResults) autoDeleteListings($str_older_dttm);
}
autoUpdateStatus();
//autoDeleteListings($listingdttm,$listingpics);
print "<tr class='headRow1'><td colspan='$otherRowSpan'><div class='pull-left' style='padding-top:10px;'>".$relanguage_tags["Showing"]." <b>$startListingNum2 - $endListingNum</b> ".__("of")." $extraMessage<b>$numResults</b> ".$relanguage_tags["Listings"].".</div>"; 

if($pageNum==1){
?>
<div class='pull-right'><?php print $relanguage_tags["Show"];?> <select name='subtype' id='reListingsPerPage2' >
<option value='<?php print "$rePartialQuery:20:$pageNum:$sortby-@@-$functionType"; ?>' <?php if($listingsPerPage==20)print " selected='selected' "; ?>>20</option>
<option value='<?php print "$rePartialQuery:40:$pageNum:$sortby-@@-$functionType"; ?>' <?php if($listingsPerPage==40)print " selected='selected' "; ?>>40</option>
<option value='<?php print "$rePartialQuery:60:$pageNum:$sortby-@@-$functionType"; ?>' <?php if($listingsPerPage==60)print " selected='selected' "; ?>>60</option>
<option value='<?php print "$rePartialQuery:80:$pageNum:$sortby-@@-$functionType"; ?>' <?php if($listingsPerPage==80)print " selected='selected' "; ?>>80</option>
<option value='<?php print "$rePartialQuery:100:$pageNum:$sortby-@@-$functionType"; ?>' <?php if($listingsPerPage==100)print " selected='selected' "; ?>>100</option>
<?php 
print "</select> ".$relanguage_tags["Listings"]."/".$relanguage_tags["page"]."";
}
print "</div></td></tr>";
if($pageNum==1)$pgClassStart="disabled";
else $pgClassStart="pgNav";
if($pageNum==$totalPages)$pgClassEnd="disabled";
else $pgClassEnd="pgNav";
$nextPage=$pageNum+1;
$prevPage=$pageNum-1;

if($pageNum<5){
	$maxNumOfPagesInNavigation=5;
	$newFirstPage=1;
}else{
	$newFirstPage=$pageNum-2;
	$maxNumOfPagesInNavigation=$pageNum+2;
}

print "<tr><td colspan='5'><div class='pull-right'><ul class='pagination'>"; ?>
<li class='<?php print $pgClassStart; ?>'><a href='javascript: void(0)'><span style="display: none;"><?php print "$rePartialQuery:$listingsPerPage:$prevPage:$sortby-@@-$functionType"; ?></span><?php print $relanguage_tags["Previous"];?></a></li>
<?php 
if($maxNumOfPagesInNavigation>$totalPages) $maxNumOfPagesInNavigation=$totalPages;
for($pg=$newFirstPage;$pg<=$maxNumOfPagesInNavigation;$pg++){
    if($pg!=$pageNum){
     ?>
    <li><a href='javascript: void(0)' class='pgNav'><span style="display: none;"><?php print "$rePartialQuery:$listingsPerPage:$pg:$sortby-@@-$functionType"; ?></span><?php print $pg; ?></a></li>
<?php }else{
     ?>
    <li class='active'><a href='javascript: void(0)'><span style="display: none;"><?php print "$rePartialQuery:$listingsPerPage:$pg:$sortby-@@-$functionType"; ?></span><?php print $pg; ?></a></li>
<?php   
    }

}

?>
<li class='<?php print "$pgClassEnd"; ?>'><a href='javascript: void(0)'><span style="display:none"><?php print "$rePartialQuery:$listingsPerPage:$nextPage:$sortby-@@-$functionType"; ?></span><?php print $relanguage_tags["Next"];?></a></li></ul></div></td></tr>
<?php
print "</tbody></table>";
}else{
	print "<h4 align='center'>".$relanguage_tags["No results found"]."</h4>";
	
}
return ob_get_clean();
}

/* Gets favorite record from the database as per listing id */
function getFavRecord($favid){
	include("config.php");
	if(trim($favid)!=""){
	$qr="select * from $reListingTable where id='$favid' ";
	$result=mysql_query($qr); 
	if(mysql_num_rows($result)) return mysql_fetch_assoc($result);
	else{
		require_once('geoplugin.class.php');
		$geoplugin = new geoPlugin();
		$geoplugin->locate();
		$vCountry=$geoplugin->countryName;
		$oodlecRegion=getOodleRegion($vCountry);
        $oodleArr=array();
		if($cladmin_settings['oodleplugin']==1 && function_exists("getOodleArray"))
		$oodleArr=convertArrayToClFormat(getOodleArray("","",$favid,"",$oodlecRegion));
		return $oodleArr[0];
	}
 }
}

/* Retrieve oodle's default currency as per the region */
function getOodleCurrency($region,$defaultCurrency){
	$currencies=array("canada"=>"$", "united states"=>"$", "usa"=>"$", "ireland"=>"€", "india"=>"Rs ", "united kingdom"=>"£");
	$region=strtolower($region);
	if(array_key_exists($region, $currencies)) return $currencies[$region];
	else $defaultCurrency;
}

/* Fetch the translation for a tag and returns it */
function __($tag){
	session_start();
	$translation=$tag;
	if(isset($_SESSION["cl_language"][$tag])) $translation=$_SESSION["cl_language"][$tag];
    elseif(isset($_SESSION["cl_language"][strtolower($tag)])) $translation=$_SESSION["cl_language"][strtolower($tag)];
    else $translation=$tag;
	return trim($translation);
}

/* Date desc sorting */
function dateDown($a, $b)
{
	$t1 = strtotime($a['dttm']);
	$t2 = strtotime($b['dttm']);
	return $t1 - $t2;
}

/* Date asc sorting */
function dateUp($a, $b)
{
	$t1 = strtotime($a['dttm']);
	$t2 = strtotime($b['dttm']);
	return $t2 - $t1;
}

/* This function sorts listing by their type i.e. normal, featured or inactive */
function listingTypeSort($a, $b){
	return $b['listing_type'] - $a['listing_type'];
}

/* Price desc sorting */
function priceDown($a, $b)
{  
	return $a['price'] - $b['price'];
}

/* Price desc sorting */
function priceUp($a, $b)
{
	return $b['price'] - $a['price'];
}

/* City desc sorting */
function cityDown($a, $b)
{
	return strcasecmp($a['city'],$b['city']);
}

/* City desc sorting */
function cityUp($a, $b)
{
	return strcasecmp($b['city'],$a['city']);
}

/* Automatically delete listings that have expired as per the limit specified in admin options */
function autoDeleteListings($str_older_dttm){
	include("config.php");
	$qrexp="select pictures from $reListingTable where 	dttm_modified <= '$str_older_dttm' and listing_type='1'";
	$resultexp=mysql_query($qrexp);
	$expcount=0;
		while($allexpPics=mysql_fetch_assoc($resultexp)){
		$thepics=explode("::",$allexpPics['pictures']);
		$totpics=sizeof($thepics);
		for($i=0;$i<$totpics;$i++) if(trim($thepics[$i])!="") unlink($thepics[$i]);
		$expcount++;
		}
	$delqr="delete from $reListingTable where dttm_modified <= '$str_older_dttm' and listing_type='1' ";
	$resultdel=mysql_query($delqr);
	
}

/* Change featured listing status to normal once featured duration is over */
function autoUpdateStatus(){
	include("config.php");
	$now_dttm=date("Y-m-d H:i:s");	
	$reqr1="update $reListingTable set listing_type='1', dttm_modified = '$now_dttm' where featured_till<='$now_dttm' and featured_till<>'0000-00-00 00:00:00' ";
	$resultre1=mysql_query($reqr1);	
    
	//print $reqr1;
}

/* Checks if a viewer has already viewed the listing */
function hasVisited($reid){
	include("config.php");
	if(trim($_SESSION["reid"])=="") if(isset($_COOKIE['reidvisit'])) $_SESSION["reid"] = $_COOKIE['reidvisit'];
	if(trim($_SESSION["marked_reid"])=="") if(isset($_COOKIE['markedreid'])) $_SESSION["marked_reid"] = $_COOKIE['markedreid'];
	
	$allreid=explode(":",$_SESSION["reid"]);
	$allMarkedreid=explode(":",$_SESSION["marked_reid"]);
	$retString="";
	
	if(in_array($reid,$allreid)) $retString="<span class='alreadySeen label label-info' title='".$relanguage_tags["You have already seen this listing"].".'>".$relanguage_tags["Viewed"]."</span>";
	if(in_array($reid,$allMarkedreid)) $retString=$retString."<span class='listingMarked label label-success' title='".$relanguage_tags["This listing has been liked by you"].".'>".$relanguage_tags["Liked"]."</span>";
	return $retString;
}

/* Saves the search preferences in SESSION variables */
function reSetSearchSession($reCategory,$reSubcategory,$rePrice,$reQuery,$reCity){
$_SESSION["reCategory"]=$reCategory;
$_SESSION["reSubcategory"]=$reSubcategory;
$_SESSION["rePrice"]=$rePrice;
$_SESSION["reQuery"]=$reQuery;
$_SESSION["reCity"]=$reCity;
}

/* Checks if a member has logged in and shows edit, delete buttons */
function showMemberNavigation($current_mem_id,$mem_id,$reid,$rePageType){
include("config.php");
if($current_mem_id!="oodle"){
if($current_mem_id==$mem_id || $_SESSION["memtype"]==9){ print "<a class='moreInfo btn btn-sm btn-info' href='index.php?ptype=editReListingForm&reid=$reid'>".$relanguage_tags["Edit Listing"]."</a>"; ?>
<span onclick="javascript:confirmListingdelete('<?php print $reid; ?>',<?php print $rePageType; ?>);" class='moreInfo btn btn-sm btn-info'><?php print $relanguage_tags["Delete Listing"]; ?></span>
<?php 
}
}
}

/* Shows registration form */
function registerUser($q){
    include("config.php");
    //list($reusername,$temp)=split(":",$q);
    if(isset($_SESSION["myusername"])){
        ?>
        Welcome <b><?php print $_SESSION["myusername"]; ?></b>
        <?php
    }else{
        /* onkeyup="if(this.value.match(/[^\w+$ ]/g)) { this.value = this.value.replace(/[^\w+$ ]/g, '');}" */
        $randnum=rand(100,10000);
        ?>
        <h3 align='center'><?php print $relanguage_tags["Please register"];?></h3>
        <form id="registerForm" name="registerForm" class="registerForm"  method="post" action="reRegister.php">
        <table border='0' style="margin:10px auto;">
        <tr><td><b><?php print $relanguage_tags["Username"];?>:</b></td><td><input id="reusername"  name="myusername" size='20'  type="text" maxlength="255" value="<?php print $_SESSION['reg_username']; ?>" onblur="infoResults(this.value,5,'usernameMessage');"  /> 
        <br /><div id='usernameMessage'></div></td></tr>
        <tr><td><b><?php print $relanguage_tags["Email"];?>:</b></td><td><input id="reemail"  name="myemail" size='20' type="text" maxlength="255" value="<?php print $_SESSION['reg_email']; ?>"/> </td></tr>
        <tr><td><b><?php print $relanguage_tags["Password"];?>:</b></td><td><input id="repassword"  name="mypassword" size='20' type='password'  maxlength="255" value=""/> </td></tr>
        <tr><td><b><?php print $relanguage_tags["Confirm"];?>:</b></td><td><input id="recpassword"  name="mycpassword" size='20' type='password'  maxlength="255" value=""/> </td></tr>
        <?php if($enableRegisterCaptcha){ ?>
        <tr><td colspan='2'>
        <img id="captcha" src="securimage/securimage_show.php?<?php print $randnum; ?>" alt="CAPTCHA Image" />
        </td></tr>
        <tr><td><b><?php print __("Enter the words"); ?></b></td><td><input type="text" name="captcha_code" id="captcha_code" size="10" maxlength="6" /></td></tr>
        <?php } ?>
        <tr><td colspan='2' align='right'><input id="registerButton2"  class='btn btn-sm btn-success' type="button" name="register" value="<?php print $relanguage_tags["Register"]; ?>" onclick="return validateRegForm()" /></td></tr>
        <tr><td colspan='2' align='right'></td></tr>
        <tr><td colspan='2' align='right'>
        <span class='small' id='loginLink2'><?php print $relanguage_tags["Already registered"];?>? <a href='javascript: void(0)'><?php print $relanguage_tags["Login here"];?></a></span><br />
        <span id='forgotPasswordLink2' class='small'><a href='javascript: void(0)'><?php print $relanguage_tags["Forgot password"]; ?>?</a></span></td></tr>
        </table>
        </form>
        <?php
    }
    
}

/* Shows login form */
function showLoginForm(){
	include("config.php");
	include("loginForm.php");
}

/* This function checks if a user with the specified username already exists */
function checkUsernameExists($q){
if(trim($q)!=""){
include("config.php");	
$qr="select id from $rememberTable where username='$q';";	
$result=mysql_query($qr);
if(mysql_num_rows($result)>0) print "<span class='redMessage'>Username not available. Please choose a different username.</span>";
}
}

/* Saves registration data in database */
function completeRegistration($q){
	include("config.php");
	include_once 'securimage/securimage.php';
	$securimage = new Securimage();
	list($reusername,$reemail,$retextpassword,$captcha_code)=explode(":::",$q,4);
    $repassword=md5($retextpassword);
    $_SESSION['reg_username']=$reusername;
    $_SESSION['reg_email']=$reemail;
	if ($securimage->check($captcha_code) == false && $enableRegisterCaptcha) {
			print __("Please enter the word challenge exactly as it appears").$captcha_code.__(" is incorrect")."."; ?>
			<br /><a href='javascript: void(0)' onclick="infoResults('register',2,'sidebarLogin');"><?php print $relanguage_tags["Please try again"]; ?></a>.
	<?php 	}else{
	$str_today = date("Y-m-d");
	$ip=$_SERVER["REMOTE_ADDR"];
	$qr="insert into $rememberTable (username,password,email,dttm,ip) values ('$reusername','$repassword','$reemail','$str_today','$ip')";
	if(mysql_query($qr)){
		print "<h3 align='center'>".$relanguage_tags["Registration successful"]."</h3>";
        sendPassword($reemail,'register',$retextpassword);
		?>
		<h5 align='center'><a href='javascript: void(0)' onclick="infoResults('login',4,'sidebarLogin');"><?php print $relanguage_tags["Login here"]; ?></a>.</h5>
		<?php 
	}else{
	 print "<h4 align='center'>".$relanguage_tags["Registration failed."]."</h4>";
	 if(mysql_errno()==1062){
	  print "<h5 align='center'>".$relanguage_tags["Account associated with"]." $reemail or $reusername ".$relanguage_tags["already exists"].".";
	 ?>
	 <br /><a href='javascript: void(0)' onclick="infoResults('register',2,'sidebarLogin');"><?php print $relanguage_tags["Please try again"]; ?></a>.
	 <?php 
	  print"</h5>";
	 }
	}
	}
}

/* Shows forgot password form */
function forgotPasswordForm($q){
    include("config.php");
    if(isset($_SESSION["myusername"])){
        ?>
            Welcome <b><?php print $_SESSION["myusername"]; ?></b>
            <?php
        }else{
            ?>
<form id="forgotPasswordForm" name="forgotPasswordForm" >
<table border='0' align='center' width='98%'>
<tr><td align='center'><b><?php print $relanguage_tags["Email"]; ?>:</b><input id="reemail"  name="myemail"  type="text" maxlength="255" value="" /></td></tr>
<tr><td align='center'><input id="forgotPasswordButton"  class='btn btn-sm btn-info' type="button" name="register" value="<?php print $relanguage_tags["Send Password"]; ?>" onclick="return processForgotPassForm();" /></td></tr>
<tr><td align='center'><br />
<span class='small' id='loginLink2'><?php print $relanguage_tags["Already registered"];?>? <a href='javascript: void(0)'><?php print $relanguage_tags["Login here"]; ?></a></span><br />
<span class='small' id='registerLink'><a href='javascript: void(0)' ><?php print $relanguage_tags["Register"]; ?>?</a></span></td></tr>
</table></form>
            <?php }
}

/* Emails the password to the member's email */
function sendPassword($email,$requestType='forgot',$mpassword=''){
include("config.php");
$qr="select * from $rememberTable where email='$email'";
$result=mysql_query($qr);
print "<p align='center'>";
if(mysql_num_rows($result)>0){
    $row=mysql_fetch_assoc($result);    
    $visitor_email=$gmailUsername;
    if($requestType=='forgot'){
    if($email=="test@finethemes.com") exit;    
    $mytextpassword=randomString(8);  
    $mypassword=md5($mytextpassword);  
    $qr2="update $rememberTable set password='$mypassword' where email='$email'";
    $result2=mysql_query($qr2);
    $msgbody=__("A forgot password request was initiated. Your login info is mentioned below with a new password").": <br /><br />
    ".__("Username").": ".$row['username']."<br />
    ".__("Password").": ".$mytextpassword."<br /><br />
    - <b>$reSiteName</b>";
    $subject=__("Password retrieval");
    print __("New password sent to your registered email").".<br /><span class='small'><a href='index.php'>".__("Login here")."</a></span></p>";
    
    }elseif($requestType=='register'){
    $msgbody=__("Thank you for registering. Your login info is mentioned below").": <br /><br />
    ".__("Username").": ".$row['username']."<br />
    ".__("Password").": ".$mpassword."<br /><br />
    - <b>$reSiteName</b>";
    $subject=__("Login Information");   
    }else{
    $msgbody=__("Your login info is mentioned below").": <br /><br />
    ".__("Username").": ".$row['username']."<br />
    ".__("Password").": ".$mpassword."<br /><br />
    - <b>$reSiteName</b>";
    $subject=__("Login Information");      
    }
    
    $to_email=$email;
    $to_name="Member";
    
sendReEmail($visitor_email,$msgbody,$to_email,$to_name,$subject,false); 
}else print "Email not found";

}

/* Generates a random string */
function randomString($length, $charset='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789')
{
    $str = '';
    $count = strlen($charset);
    while ($length--) {
        $str .= $charset[mt_rand(0, $count-1)];
    }
    return $str;
}


/* This function fetches member's profile image and shows it */
function showProfileImage($q){
include("config.php");
$mem_id=$_SESSION["re_mem_id"];
if($q!="no"){
$qr="select * from $rememberTable where id='$mem_id'";
$result=mysql_query($qr);
$row=mysql_fetch_assoc($result);
if($result) if(trim($row['photo'])!="") print "<img src='uploads/".$row['photo']."' width='200' />";
}else{
print "";
}
}

/* Function called when a listing is liked by a viewer */
function markReListing($reid){
	include("config.php");
	$_SESSION["marked_reid"]=$reid.":".$_SESSION["marked_reid"];
	$reCookieTime = 60 * 60 * 24 * 12 + time();
	setcookie('markedreid', $_SESSION["marked_reid"], $reCookieTime);
	print '<span class="btn btn-sm btn-success">'.$relanguage_tags["Listing marked"]."</span>";
}

/* Deletes a listing when a member deletes a listing */
function deleteReListing($reid){
  if(isset($_SESSION["myusername"])){
	include("config.php");
	if($_SESSION["memtype"]==9){
        $qr0="select pictures from $reListingTable where id='$reid' ";
        $deleteClause="";
    }else{
        $qr0="select pictures from $reListingTable where id='$reid' and user_id='".$_SESSION["re_mem_id"]."'";
        $mem_id=$_SESSION["re_mem_id"];
        $deleteClause=" and user_id='$mem_id' ";
    }
	$result0=mysql_query($qr0);
	$row0=mysql_fetch_assoc($result0);
	$allPictures=explode("::",$row0['pictures']);
	$totalPics=sizeof($allPictures);
	
	$qr="delete from $reListingTable where id='$reid' $deleteClause";
	$result=mysql_query($qr);
	if($result){
		for($i=0;$i<$totalPics;$i++){
		if(trim($allPictures[$i])!="")
		list($temppart,$actualImgName)=explode("uploads/",$allPictures[$i]);
		unlink("uploads/".$actualImgName);
		//print "deleting: ".$actualImgName;
		}
		exit;
		print "<h5 align='center'>Listing # $reid deleted.</h5>";
	}
	
  }else print "Please sign in"; 
	
}

/* Delete a listing picture when a member delets it */
function deleteRePhoto($qr){
include("config.php");
list($reimgid,$reid)=explode(":::",$qr);
list($temppart,$actualImgName)=explode("uploads/",$reimgid);
if(unlink("uploads/".$actualImgName)) print "<h4 align='center'>".$relanguage_tags["Image Deleted"].".</h4>";
else print "<h4 align='center'>Picture $reimgid couldn't be deleted. Please check if the file exists and permissions of the upload folder</h4>";

$reqr1="select pictures from $reListingTable where id='$reid' ";
$resultre1=mysql_query($reqr1);
$row=mysql_fetch_assoc($resultre1);
$allpictures=$row['pictures'];
if(trim($reimgid)!=""){
	//$full_url_path = "http://" . $_SERVER['HTTP_HOST'] . preg_replace("#/[^/]*\.php$#simU", "/", $_SERVER["PHP_SELF"]);
	$allpictures=str_replace("$reimgid::", "", $allpictures);
}
$reqr2="update $reListingTable set pictures='$allpictures'  where id='$reid' ";
$resultre2=mysql_query($reqr2);
//print "<br /><br />$reimgid"."<br /><br />$reqr2<br />";
}

/* Changes member status to active, ban or deleted */
function changeMemberStatus($q){
include("config.php");
list($memstatus,$rememid)=explode("::",$q);
if($memstatus=="Ban" || $memstatus=="Active"){
$reqr1="update $rememberTable set status='$memstatus' where id='$rememid' ";
$resultre1=mysql_query($reqr1);
if($resultre1){
if($memstatus=="Ban")	print "Banned";
if($memstatus=="Active")	print "Activated";
}
}
if($memstatus=="Delete"){	
	$reqr1="delete from $rememberTable where id='$rememid' ";
	$resultre1=mysql_query($reqr1);
	if($resultre1){
		print "Deleted";
	}	
}
}

/* Changes listing status to normal, featured or inactive */
function changeListingStatus($q){
    include("config.php");
    if($isThisDemo=="yes"){ print "Listing status can't be updated in demo"; exit; }
    list($listingstatus,$reid)=explode("::",$q);    
    if($listingstatus==2){
        $dt_now = date("Y-m-d");
        $now_month = date("n");
        $now_day = date("j");
        $now_year = date("Y");
        $now_hour= date("H");
        $now_minute= date("i");
        $now_second= date("s");
        if($featuredduration=="" || $featuredduration==0) $featuredduration=30;
        $minmonth = mktime($now_hour, $now_minute, $now_second, $now_month, $now_day + $featuredduration ,  $now_year );
        $future_dttm = date("Y-m-d H:i:s",$minmonth);
        $reqr1="update $reListingTable set listing_type='$listingstatus', featured_till='$future_dttm' where id='$reid' ";
    }
    else{ $reqr1="update $reListingTable set listing_type='$listingstatus' where id='$reid' "; }
    $resultre1=mysql_query($reqr1); 
    if($listingstatus==1) $listingstatusString="normal";
    if($listingstatus==2) $listingstatusString="featured";
    if($listingstatus==3) $listingstatusString="Inactive";
    if($isThisDemo=="yes" && $listingstatus==3){
    print "<br />Listing status can't be changed to 'Inactive' in the demo.";    
    }else { if($resultre1) print "Status changed to $listingstatusString"; }
     
    
}

/* Gets ajax request and pass on to other functions as per user's action */
function safelyExecute($q,$type){

	try{
		
	include("config.php");
	$con=mysql_connect($host,$username,$password) or die("Could not connect. Please try again.");
	mysql_select_db($database,$con);
	//mysql_query("SET character_set_results=utf8");
	mysql_query("SET NAMES utf8");
	$q=mysql_real_escape_string($q);
	
	if($type==1) searchResults($q);
	if($type==2) registerUser($q);
	if($type==3) completeRegistration($q);
	if($type==4) showLoginForm();
	if($type==5) checkUsernameExists($q);
	if($type==6) showProfileImage($q);
	if($type==7) searchResults($q,"yes");
	if($type==8) markReListing($q);
	if($type==9) forgotPasswordForm($q);
	if($type==10) sendPassword($q);
	if($type==11) deleteReListing($q);
	if($type==12) deleteRePhoto($q);
	if($type==13) changeMemberStatus($q);
	if($type==14) changeListingStatus($q);
	if($type==15) changeStatus($q);
	if($type==16) addcategory($q);
	if($type==17) subcategorysection($q);
	if($type==18) addsubcategory($q);
	if($type==19) removecategory($q);
	if($type==20) removesubcategory($q);
	if($type==21) showsubcatprice($q);
	if($type==22) addNewTag($q);
	if($type==23) showMapListing($q);
	if($type==24) searchResults($q,"no","yes");
	if($type==25) acode($q);
    if($type==26) updateLangTag($q);
    if($type==27) setJSvariables($q);
    if($type==28) flagListing($q);
    
	}catch(Exeption $e){ print "Function Type - $type : ".$e; }
	mysql_close($con);

}

/* Flag a listing */
function flagListing($listing_id){
    $ip=$_SERVER["REMOTE_ADDR"];
    
    $qr="select * from flagging where ip='$ip' and listing_id='$listing_id'";
    $result=mysql_query($qr); 
    if($result){
        if(mysql_num_rows($result) <= 0){
            $qr2="insert into flagging (listing_id,ip) values ('$listing_id','$ip')"; 
            $result2=mysql_query($qr2);
            $qr3="update listing set flag=flag+1 where id='$listing_id'";
            $result3=mysql_query($qr3); 
            if($result2 && $result3){
                print '<span class="btn btn-warning" disabled="disabled">'.__("Listing reported")."</span>";
            }
        }else{
            print '<span class="btn btn-warning" disabled="disabled">'.__("Listing reported")."</span>";
        }
    }
    
}

/* Set session for various javascript variables */
function setJSvariables($q){
   $allVariables=explode("::",$q); 
   foreach($allVariables as $varid => $varval){
   list($variableName,$variableValue)=explode(":",$varval);    
   $_SESSION[$variableName]=$variableValue;
   }
   
}

/* Fetches complete listing data of the listing identified by $reid */
function showMapListing($reid){
	include("config.php"); $ptype="showOnMap";
	list($reid,$region)=explode("::",$reid); 
	if($region==="") $viewListingRow=getListingData($reid);	
	else $viewListingRow=getListingData($reid,$region);
    $showMoreListings="no";
	include("viewFullListing.php");
		 
}

/* Test function */
function getMapListingTest($reid){
    ob_start();
    include("config.php"); $ptype="showOnMap";
    list($reid,$region)=explode("::",$reid); 
    if($region==="") $viewListingRow=getListingData($reid); 
    else $viewListingRow=getListingData($reid,$region);
    include("viewFullListing.php");
    $string = ob_get_clean();
    return json_encode($string);
  
}

/* Gets authorization code value */
function acode($q=""){
include("config.php");
$qr0="select authorization_code from $adminOptionTable";
$result0=mysql_query($qr0); 
$record=mysql_fetch_assoc($result0);
print $record['authorization_code'];   
}

/* Changes the translation of a language tag */
function updateLangTag($q){
    include("config.php");
    list($keyword,$translation,$defaultLanguage)=explode(":::",$q); $translation=trim($translation);
    $qr00="select * from $languageTable where translation='$translation' and keyword <> '$keyword' and language='$defaultLanguage'"; 
    $result00=mysql_query($qr00);
    if(mysql_num_rows($result00)<1){
     $qr="update $languageTable set translation='$translation' where keyword = '$keyword' and language='$defaultLanguage'";  
     if($isThisDemo!="yes") $result=mysql_query($qr); 
     if($result) print "Translation updated"; else print "Translation couldn't be updated.";
    }else{
        $row00=mysql_fetch_assoc($result00);
        print "<p align='center'>Same translation already exist for the keyword <b>'".$row00['keyword']."'</b>. Please use a unique translation.<p>";
    }
}

/* Add a new tag with its translation */
function addNewTag($q){
	include("config.php");
	list($keyword,$translation,$defaultLanguage)=explode(":::",$q);
    $qr00="select * from $languageTable where translation='$translation' and language='$defaultLanguage'"; 
    $result00=mysql_query($qr00);
    if(mysql_num_rows($result00)<1){
	$qr0="select * from $languageTable where keyword='$keyword' and language='$defaultLanguage'";
	$result0=mysql_query($qr0);
	$qr2="insert into $languageTable (keyword,translation,language) values ('$keyword','$translation','$defaultLanguage')";
	
	if($isThisDemo=="no"){
	if(@mysql_num_rows($result0)<1){
	$result2=mysql_query($qr2);
	if($result2){
		print "<p align='center'>New keyword and translation ".stripslashes($category)." has been added.</p>";
		unset($_SESSION["cl_language"]);
	}else print "<p align='center'>Keyword and translation ".stripslashes($category)." couldn't be added. Please try again.</p>";
	}else{
		print "<p align='center'>The keyword and translation ".stripslashes($category)." already exists. ".mysql_error()."</p>";
	}
	}else print "Adding a new keyword and translation has been disabled in the demo.";
	}else{
	    $row00=mysql_fetch_assoc($result00);
        print "<p align='center'>Same translation already exist for the keyword <b>'".$row00['keyword']."'</b>. Please use a unique translation.<p>";
	}
}

/* Deletes a category name from the category list */
function removecategory($category){
	include("config.php");
	if($isThisDemo=="no"){
	if(trim($category)!=""){
		$qr2="delete from $categoryTable where category='$category'";
		$result2=mysql_query($qr2);
		if($result2) print "<p align='center'>Category ".stripslashes($category)." has been deleted.</p>";
		else print "<p align='center'>Category ".stripslashes($category)." couldn't be deleted.</p>";

		$qr3="select * from $categoryTable";
		$result3=mysql_query($qr3);
		$totalcats=mysql_num_rows($result3);
		if($totalcats>0){
			$i=0;
			?>
			<select id='categoryselect' name='categoryselect' size='<?php print $totalcats; ?>' onclick="infoResults(this.value,17,'subcatsection');" >
			<?php 
				while($allcategories=mysql_fetch_assoc($result3)){
				print "<option name='cat-$i' value='".htmlspecialchars($allcategories['category'], ENT_QUOTES)."'>".$allcategories['category']."</option>";
				$i++;	
				}
		   print "</select>";
		 
			}
			
	}
	}else{
	print "Removing a category has been disabled in the demo.";
	}
}

/* Deletes a subcategory name from the category list */
function removesubcategory($q){
	include("config.php");
	if($isThisDemo=="no"){
	list($subcategory,$category)=explode(":::",$q);
	//print "$subcategory,$category";
	
	if(trim($category)!=""){
		$qr2="select * from $categoryTable where category='$category'";
		$result2=mysql_query($qr2);
		$allsubcats=mysql_fetch_assoc($result2);
		$allsubcatsarray=explode(":::",$allsubcats['subcategories']);
		//$allsubcatsPricearray=explode(":::",$allsubcats['price']);
		$allsubcatsize=sizeof($allsubcatsarray);
		for($i=0;$i<$allsubcatsize;$i++){
			$allsubcatsarray[$i]=mysql_real_escape_string($allsubcatsarray[$i]);
			if(trim($allsubcatsarray[$i])==$subcategory){
				unset($allsubcatsarray[$i]);
				//unset($allsubcatsPricearray[$i]);
			}
			//print $allsubcatsarray[$i]." ".$subcategory."<Br />";
		}
		$allsubcats['subcategories']=implode(":::",$allsubcatsarray);
		//$allsubcats['price']=implode(":::",$allsubcatsPricearray);
		
		$qr3="update $categoryTable set subcategories='".$allsubcats['subcategories']."' where category='$category'";
		$result3=mysql_query($qr3);
		if($result3) print "<p align='center'>Sub category ".stripslashes($subcategory)." has been deleted.</p>";
		else print "<p align='center'>Sub category ".stripslashes($subcategory)." couldn't be deleted.</p>";	
		//print $qr3;".stripslashes($subcat)."
		
		$qr3="select * from $categoryTable where category='$category'";
		$result3=mysql_query($qr3);
		$totalcats=mysql_num_rows($result3);
		if($totalcats>0){
			$allsubcats=mysql_fetch_assoc($result3);
			$allsubcategories=explode(":::",$allsubcats['subcategories']);
			$subcatsize=sizeof($allsubcategories);
			if(strlen($allsubcats['subcategories'])>0){
				?>
		 	<select id='subcategoryselect' name='subcategoryselect' size='<?php print $subcatsize; ?>' >
			<?php 
			 for($i=0;$i<$subcatsize;$i++){
			 print "<option name='subcat-$i' value='".htmlspecialchars($allsubcategories[$i], ENT_QUOTES)."'>".$allsubcategories[$i]."</option>";
				}
		   print "</select><br /><br /><br />";
		
		 }
		}
		
}
	}else{
		print "Removing a sub category has been disabled in the demo.";
	}
}

/* Adds a Category name from the category list */
function addcategory($category){
	include("config.php");
	
	list($category,$catprice)=explode(":::",$category);
	if(trim($category)!="" && trim($category)!="0"){
	$reqr1="select * from $categoryTable where category='$category' ";
	$resultre1=mysql_query($reqr1);	
	
	if(mysql_num_rows($resultre1)>0){
		print "<p align='center'>Category ".stripslashes($category)." already exists.</p>";
	}else{
		if($isThisDemo=="no"){
		$qr2="insert into $categoryTable (category,price) values ('$category','$catprice')";
		$result2=mysql_query($qr2);
		if(strtolower($redefaultLanguage)=="english") addNewTag("$category:::$category:::$redefaultLanguage");
		if($result2) print "<p align='center'>Category ".stripslashes($category)." has been added.</p>";
		else print "<p align='center'>Category ".stripslashes($category)." couldn't be added.</p>";
		}else{
			print "Adding a category is disabled in the demo.";
		}
	}
	}
	$qr3="select * from $categoryTable";
	$result3=mysql_query($qr3);
	$totalcats=mysql_num_rows($result3);
	if($totalcats>0){
		$i=0;
		?>
		<br /><br />
		<b>Existing Categories</b><br />
		<div id='categorylist'>
	<select id='categoryselect' name='categoryselect' size='<?php print $totalcats; ?>' onclick="infoResults(this.value,17,'subcatsection');" >
	<?php 
		while($allcategories=mysql_fetch_assoc($result3)){
		print "<option name='cat-$i' value='".htmlspecialchars($allcategories['category'], ENT_QUOTES)."'>".$allcategories['category']."</option>";
		$i++;	
		}
   print "</select></div>";
   ?>
  
   <?php
   print "<br /><br /><div id='subcatsection'></div>";
	}
	
}

/* Shows subcategory related options in search form */
function subcategorysection($category){
	?>
<input type='button' value='Remove Selected Category' id='removecat' onclick="if(confirm('Do you really want to delete category <?php print $category; ?>. All sub categories under this category would also be deleted.')) infoResults('<?php print $category; ?>',19,'categorylist');"  />
	<br /><br />
	<?php 
print "<br /><b>Add a sub category for $category</b><br />";
?>
<input type='text' name='subcatname' id='subcatname' size='35' /><br /><br />
<input type='button' id='addsubcat' onclick="infoResults(document.getElementById('subcatname').value+':::'+'<?php print $category; ?>',18,'subcatlist');" value='Add Sub Category' />
<br /><br />
<div id='allsubcats'>
<div id='subcatlist'>
<?php 
include("config.php");
$qr3="select * from $categoryTable where category='$category'";
$result3=mysql_query($qr3);
$totalcats=mysql_num_rows($result3);
$allsubcats=mysql_fetch_assoc($result3);
?>
Is the price field enabled for category <b><?php print $category; ?></b>: <?php print $allsubcats['price']; ?><br /><br />
<?php 
if($totalcats>0){
	
	$allsubcategories=explode(":::",$allsubcats['subcategories']);
	$subcatsize=sizeof($allsubcategories);
	if(strlen($allsubcats['subcategories'])>0){
?>
  
   <b>Existing sub categories</b><br /><br />
	<select id='subcategoryselect' name='subcategoryselect' size='<?php print $subcatsize; ?>'  >
	<?php 
	//$tstring="a\'test";
	//$tstring=htmlspecialchars($tstring, ENT_QUOTES);
	 for($i=0;$i<$subcatsize;$i++){
		print "<option name='subcat-$i' value='".htmlspecialchars($allsubcategories[$i], ENT_QUOTES)."'>".$allsubcategories[$i]."</option>";
			
		}
   print "</select><br /><br />";
   ?>
   </div></div>
   <input type='button' value='Remove Selected Sub Category' id='removecat' onclick="infoResults(document.getElementById('subcategoryselect').value+':::'+'<?php print $category; ?>',20,'subcatlist');" />
    
     <?php 
 }
}
?>

<?php 	
}

/* Adds a Subcategory name from the category list */
function addsubcategory($q){
include("config.php");

list($subcat,$category)=explode(":::",$q);

if(trim($subcat)!=""){
$qr3="select * from $categoryTable where category='$category'";
$result3=mysql_query($qr3);
$totalcats=mysql_num_rows($result3);

if($totalcats>0){
	$allsubcats=mysql_fetch_assoc($result3);
	$allsubcategories=explode(":::",$allsubcats['subcategories']);
	$subcatsize=sizeof($allsubcategories);
	
	if(strpos($allsubcats['subcategories'], $subcat)===false){
	if(trim($allsubcats['subcategories'])!=""){
		$allsubcats['subcategories']=mysql_real_escape_string($allsubcats['subcategories']).":::".$subcat;
		//$allsubcats['price']=mysql_real_escape_string($allsubcats['price']).":::".$price;
	}
	else{
		$allsubcats['subcategories']=$subcat;
		//$allsubcats['price']=$price;
	}
	if($isThisDemo=="no"){
	$qr4="update $categoryTable set subcategories='".$allsubcats['subcategories']."' where category='$category'";
	$result4=mysql_query($qr4);
	//print $qr4;
	if(strtolower($redefaultLanguage)=="english") addNewTag("$subcat:::$subcat:::$redefaultLanguage");
	if($result4) print "<p align='center'>Sub category ".stripslashes($subcat)." has been added to $category.</p>";
	else print "<p align='center'>Sub category ".stripslashes($subcat)." couldn't be added to $category.</p>";
	$newSubCat="<option name='subcat-$i' value='".htmlspecialchars($subcat, ENT_QUOTES)."'>".stripslashes($subcat)."</option>";
	$optsize=$subcatsize+1;
	}else{
		print "Adding a sub category is disabled in the demo.";
		$optsize=$subcatsize;
	}
	}else{
		print "<b>Sub category $subcat exists.</b><br /><br />";
		$newSubCat="";
		$optsize=$subcatsize;
	}
	
?> <div id='subcatlist'>
	<select id='subcategoryselect' name='subcategoryselect' size='<?php print $optsize; ?>' >
	<?php 
	 for($i=0;$i<$subcatsize;$i++){
		print "<option name='subcat-$i' value='".htmlspecialchars($allsubcategories[$i], ENT_QUOTES)."'>".stripslashes($allsubcategories[$i])."</option>";
	  }
		print $newSubCat;
   print "</select><br /><br />";
   ?>
   </div>
  <!-- <input type='button' value='Remove Selected Sub Category' id='removecat' onclick="infoResults(document.getElementById('subcategoryselect').value+':::'+'<?php print $category; ?>',20,'allsubcats');" />-->
    
   <?php 
	
		
 }
}else{
	print "<b>Please enter sub category name.</b>";
}

}

/* Returns parsed data to be used in mysql queries a per their type */
function getRealValue($value,$type){
	//print "$type=$value<br />";
	include("config.php");
	//print "value= $value,$type - ".$relanguage_tags["Any"]."<Br />";
	
	if($type=="reCategory"){
		$allValues=explode(",",$value);
		$totSize=sizeof($allValues);
		for($j=0;$j<$totSize;$j++){
			$allValues[$j]=__($allValues[$j]);
		 }		 
		//print "<br />categories ($value)<br />";
		//print_r($allValues);
		for($i=0;$i<$totSize;$i++){
			if($allValues[$i]==$relanguage_tags["Any"] || $allValues[$i]=="" || $allValues[$i]=="Any"){
				$retString=$retString." and category like '%'";
				break;
			}else{
				if($retString=="") $delimClause=" and ( ";
				else $delimClause=" or ";
				$retString=$retString." $delimClause category='".$allValues[$i]."' ";
			}
			
		}
		if($retString!="" && $retString!=" and category like '%'") $retString=$retString." ) ";
		return $retString;
		
	}
	
	if($type=="reSubcategory"){
		$allValues=explode(",",$value);
		$totSize=sizeof($allValues);
		
		for($j=0;$j<$totSize;$j++){
			$allValues[$j]=__($allValues[$j]);
		}
        //print_r($allValues);
		for($i=0;$i<$totSize;$i++){
			if($allValues[$i]==$relanguage_tags["Any"] || $allValues[$i]=="" || $allValues[$i]=="Any"){
				$retString=$retString." and subcategory like '%'";
				break;
			}
			else{
				if($retString=="") $delimClause=" and ( ";
				else $delimClause=" or ";
				$retString=$retString." $delimClause subcategory='".$allValues[$i]."' ";
			}
				
		}
		if($retString!="" && $retString!=" and subcategory like '%'") $retString=$retString." ) ";
		return $retString;
	
	}
	
    if($type=="activities"){
        $allValues=explode(",",$value);
        $totSize=sizeof($allValues);
        //print_r($allValues);
        for($j=0;$j<$totSize;$j++){
            $allValues[$j]=__($allValues[$j]);
        } 
        for($i=0;$i<$totSize;$i++){
            if($allValues[$i]==$relanguage_tags["Any"] || $allValues[$i]=="" || $allValues[$i]=="Any"){
                $retString=$retString." and activities like '%'";
                break;
            }
            else{
                if($retString=="") $delimClause=" and ( ";
                else $delimClause=" or ";
                $retString=$retString." $delimClause activities='".$allValues[$i]."' ";
            }
                
        }
        if($retString!="" && $retString!=" and activities like '%'") $retString=$retString." ) ";
        return $retString;
    
    }
    
    if($type=="types"){
        $allValues=explode(",",$value);
        $totSize=sizeof($allValues);
        //print_r($allValues);
        for($j=0;$j<$totSize;$j++){
            $allValues[$j]=__($allValues[$j]);
        }
        for($i=0;$i<$totSize;$i++){
            if($allValues[$i]==$relanguage_tags["Any"] || $allValues[$i]=="" || $allValues[$i]=="Any"){
                $retString=$retString." and types like '%'";
                break;
            }
            else{
                if($retString=="") $delimClause=" and ( ";
                else $delimClause=" or ";
                $retString=$retString." $delimClause types='".$allValues[$i]."' ";
            }
                
        }
        if($retString!="" && $retString!=" and types like '%'") $retString=$retString." ) ";
        return $retString;
    
    }
	if($type=="rePrice"){
		$allValues=explode(",",$value);
		$totSize=sizeof($allValues);
		for($i=0;$i<$totSize;$i++){
			if($allValues[$i]==10 || $allValues[$i]==""){
				$retString=$retString." and price like '%' ";
				break;
			}
			else{
				//print "$totSize - $fromPrice,$toPrice<br />";
				list($fromPrice,$toPrice)=explode("-",$allValues[$i]);
				$fromPrice=str_replace("K", "000", $fromPrice);
				$fromPrice=str_replace("M", "000000", $fromPrice);
				$toPrice=str_replace("K", "000", $toPrice);
				$toPrice=str_replace("M", "000000", $toPrice);
				$fromPrice=str_replace("k", "000", $fromPrice);
				$fromPrice=str_replace("m", "000000", $fromPrice);
				$toPrice=str_replace("k", "000", $toPrice);
				$toPrice=str_replace("m", "000000", $toPrice);
				
				if($retString=="") $delimClause=" and ( ";
				else $delimClause=" or ";
				
				if($toPrice==__("Above")){
					$retString=$retString." $delimClause  ( price >= $fromPrice or price = 0 ) ";
					//$value=" and ( price >= $fromPrice or price = 0 ) ";
				}else{
					$retString=$retString." $delimClause  ( price <= $toPrice and price >= $fromPrice) ";
					//$value=" and ( price <= $toPrice and price >= $fromPrice or price = 0 ) ";
				}
			}
	
		}
		if($retString!="" && $retString!=" and price like '%' ") $retString=$retString." ) ";
		return $retString;
	
	}
	
	if($type=="reQuery"){
	if($value=="") $value="";
	else $value=" and ( address like '%$value%' OR country like '%$value%' OR state like '%$value%' OR description like '%$value%' OR headline like '%$value%' OR id like '%$value%'  OR postal like '%$value%' )";
	return $value;			
	}
	
	if($type=="reCity"){
		if($value=="") $value="";
		else $value=" and ( city like '%$value%' )";
		return $value;
	}
	
	if($type=="onlyMemberListings"){
		$mem_id=$_SESSION["re_mem_id"];
		if($value=="no") $value="";
		if($value=="yes") $value=" and user_id='$mem_id' ";
		return $value;
	}
	
}

/* Sets uid in admin options table of the database */
function changeStatus($q){
include("config.php");
$qr="update $adminOptionTable set uid='$q';";
$result=mysql_query($qr);
print "done";
}

/* Returns comma separated string from an array. Just like implode. */
function getCommaStringFromArray($theArray){
	$theString="";
	$delim=",";
	if(!empty($theArray)){
	foreach($theArray as $thename=>$thevalue){
		if($theString!="")$delim=","; else $delim="";
		$theString=$theString.$delim.$thevalue;
	}	
	}
	return $theString;
}

/* Outputs category and related subcategories */
function getCatsSubcats($q=""){
   include("config.php");
	//print "tq=$q";
	$con=mysql_connect($host,$username,$password) or die("Could not connect. Please try again.");
	mysql_select_db($database,$con);
	mysql_query("SET NAMES utf8");
	$reqr1="select * from $categoryTable";
	$resultre1=mysql_query($reqr1);
	$jsArray="";
	print ' var catSubcats=new Array(); ';
	print ' var catSubcatsPrice=new Array(); ';
	while($allCategories=mysql_fetch_assoc($resultre1)){
		$temSubCats=explode(":::",$allCategories['subcategories']);
		$totalSubCats=sizeof($temSubCats);
		for($i=0;$i<$totalSubCats;$i++){
			$temSubCats[$i]=__($temSubCats[$i]); 
		} 
		$allCategories['subcategories']=implode(":::",$temSubCats);
		if($jsArray==""){
			print ' catSubcats["'.__($allCategories['category']).'"]="'.$allCategories['subcategories'].'"; ';
		}
		else{
			print ' catSubcats["'.__($allCategories['category']).'"]=catSubcats["'.__($allCategories['category']).'"]+":::"+"'.$allCategories['subcategories'].'"; ';
		}
		
		print ' catSubcatsPrice["'.__($allCategories['category']).'"]="'.$allCategories['price'].'"; ';
	}
	if($showPrice=="true") print " var showPriceField='true'; ";
	
}

/* Returns exact oodle region */
function getOodleRegion($region){
	$regions=array("canada"=>"canada", "united states"=>"usa", "ireland"=>"ireland", "india"=>"india", "united kingdom"=>"uk");
	$region=strtolower($region);
	if(array_key_exists($region, $regions)) return $regions[$region];
	else "no";
}

/* Sample function to be used with api */
function getMarkersJson1(){
include("config.php"); 
$con=mysql_connect($host,$username,$password) or die("Could not connect. Please try again.");
mysql_select_db($database,$con);
mysql_query("SET NAMES utf8");   
$qr="select id, latitude as la, longitude as lo from $reListingTable";
$result=mysql_query($qr);
$count=0;
   $markers=array();
    while($marker=mysql_fetch_assoc($result)){
     if($customMarkers) $marker['ca']=strtolower(str_replace(" ", "-", $marker['ca'])); 
     if($marker['lt']==1) unset($marker['lt']);   
     $markers[] = $marker;
     //$count++; if($count>=1000) break;
     }
 return json_encode($markers);   
}

/* Returns json marker data */
function getMarkersJson2(){
    include("config.php");
    $con=mysql_connect($host,$username,$password) or die("Could not connect. Please try again.");
    mysql_select_db($database,$con);
    mysql_query("SET NAMES utf8");
    $reCategory=$_GET['category'];
    $reSubcategories=$_GET['subcategories'];
    $activities=$_GET['activities'];
    $types=$_GET['types'];
    $reQuery=mysql_real_escape_string($_GET['requery']);
    $reCity=mysql_real_escape_string(trim($_GET['city']));
    $favorite=$_GET['favorite'];
    if($customMarkers) $customMarkerClause=", category as ca "; else $customMarkerClause="";
        
    reSetSearchSession($reCategory,$reSubcategories,$rePrice,$reQuery,$reCity); 
   if($favorite==1){
       $allfavs=rtrim(str_replace(":", ",", $_SESSION["marked_reid"]),',');
       $qr0="select latitude as la,longitude as lo, listing_type as lt $customMarkerClause from $reListingTable where id IN ($allfavs);";
     }else{   
    $qr0="select latitude as la,longitude as lo, listing_type as lt, types as tp $customMarkerClause from $reListingTable where listing_type <> 3  ".getRealValue($reCategory,"reCategory").getRealValue($reSubcategories,"reSubcategory")
    .getRealValue($activities,"activities").getRealValue($types,"types").getRealValue($reQuery,"reQuery").getRealValue($reCity,"reCity");
     }
  // print "$reSubcategories<br />".$qr0;
    $result0=mysql_query($qr0);
    $totalRows=mysql_num_rows($result0);
  
    $markers=array();
    $atypes=array();
    
    while($marker=mysql_fetch_assoc($result0)){
     if(!isset($allmarkers[$marker['la'].",".$marker['lo']])) $allmarkers[$marker['la'].",".$marker['lo']]=0;    
     else $allmarkers[$marker['la'].",".$marker['lo']]=$allmarkers[$marker['la'].",".$marker['lo']]+1; 
     /*
     if($allmarkers[$marker['la'].",".$marker['lo']]>0){
       foreach($markers as $key => $tempmarker){
            if ($tempmarker['la'] == $marker['la'] && $tempmarker['lo'] == $marker['lo'] ){
                unset($markers[$marker['la'].",".$marker['lo']]); 
                $markers[$marker['la'].",".$marker['lo']]["tp"]=$markers[$marker['la'].",".$marker['lo']]["tp"]."::".$marker['tp'];
            }
       }
     }   
     */
     if($customMarkers) $marker['ca']=strtolower(str_replace(" ", "-", $marker['ca'])); 
     if($marker['lt']==1) unset($marker['lt']);   
     $marker['tp']=trim($marker['tp']);
     //print "\n".$marker['la'].",".$marker['lo']."(".$marker['tp']."): ".in_array($marker['tp'], $atypes[$marker['la'].",".$marker['lo']]);    
     if(!in_array($marker['tp'], $atypes[$marker['la'].",".$marker['lo']])){
         $atypes[$marker['la'].",".$marker['lo']][]=$marker['tp'];
         $marker['tp']=implode("||",$atypes[$marker['la'].",".$marker['lo']]);
         $markers[$marker['la'].",".$marker['lo']] = $marker;
     }
     
     
     }
 //return json_encode($atypes);
 
 $markers=call_plugin("allMarkers",$markers);

 if($cladmin_settings['oodleplugin']==1 && function_exists("getOodleArray")){  
 $oodleListingArray=getOodleListingsArray($reCategory,$reSubcategories,$reQuery,$reCity,$oodlecRegion,$reMaxPictures,$attr);
 $formattedOodleArray=convertArrayToClFormat2($oodleListingArray);
 $combArray=array_merge($markers,$formattedOodleArray);
 return json_encode($combArray);
 }else return json_encode($markers);
 
}

/* Gets listing data related to the marker */
function getMarkerInfo($latitude,$longitude,$list_id,$region="usa"){
include("config.php");
$row0=array();

if(trim($list_id)=="" || $list_id==0){
$con=mysql_connect($host,$username,$password) or die("Could not connect. Please try again.");
mysql_select_db($database,$con);
mysql_query("SET NAMES utf8");

$reCategory=$_SESSION["reCategory"];
$reSubcategories=$_SESSION["reSubcategory"];
$rePrice=$_SESSION["rePrice"];
$reQuery=$_SESSION["reQuery"];
$reCity=$_SESSION["reCity"];

$qr="select * from $reListingTable where listing_type <> 3 and latitude like '$latitude%' and longitude like '$longitude%' ".getRealValue($reCategory,"reCategory").getRealValue($reSubcategories,"reSubcategory")   .getRealValue($rePrice,"rePrice").getRealValue($reQuery,"reQuery").getRealValue($reCity,"reCity") ;
$result=mysql_query($qr);
$markerContent=$listingDelimeter="";
$totalResults=mysql_num_rows($result);
while($listingTemp=mysql_fetch_assoc($result)) $listing[]=$listingTemp;
$region=$regionClause1=$regionClause2="";
}else{
$region="usa";
$combArray=convertArrayToClFormat(getOodleArray("","",$list_id,"",$region));
$oodleListing=$combArray[0];
$oodleListing['category']=ucfirst($oodleListing['category']);   
$listing[]=$oodleListing;
$regionClause1="::$region";
$regionClause2="&region=$region";
}

if($totalResults>1) $markerContent="<div class='label label-primary' style='font-size: 125%; text-align:center;'>$totalResults ".__("Listings Found Here")."</div><br /><br />";
$count=0;

$listing=call_plugin("markerRecord",$listing);

foreach($listing as $lid=>$row0){
if($row0['listing_type']==2){ $featuredClass=" style='background-color:#F9FDF8;' "; $featuredTag="<span class='featuredlisting label label-primary'>".__("Featured")."</span>"; }
else{ $featuredClass=""; }

if($totalResults>1){
    $headlineLength=40;
    $descriptionSize=170;
    $thumbnailHeight=60;
    $markerHeightStyle=" style='min-height:140px; ' ";
}else{
    $headlineLength=40;
    $descriptionSize=350;
    $thumbnailHeight=120;
}

if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true){
    $thumbnailHeight=60;
}

//$smallHeadline=addslashes(utf8_substr(escapeJavaScriptText($row0['headline']),0,$headlineLength))."....";
//$smallDescription=addslashes(nl2br(utf8_substr(preg_replace( '/\s+/', ' ', escapeJavaScriptText($row0['description'])),0,$descriptionSize)))."....";

if(isset($_SESSION["winwidth"]) && $_SESSION["winwidth"]<=1170){ $descriptionSize=120; }
if(isset($_SESSION["winwidth"]) && $_SESSION["winwidth"]<=768){ $headlineLength=20; $descriptionSize=60; }

$smallHeadline=nl2br(utf8_substr(preg_replace( '/\s+/', ' ', escapeJavaScriptText($row0['headline'])),0,$headlineLength));
$smallDescription=nl2br(utf8_substr(preg_replace( '/\s+/', ' ', escapeJavaScriptText($row0['description'])),0,$descriptionSize))."....";

$row0['address']=addslashes(escapeJavaScriptText($row0['address']));

if($row['user_id']!="oodle") $rePicArray=explode("::",$row0['pictures']);
else{
    $row0=$combArray[0];
    $rePicArray=explode("::",$row0['pictures']);
    $totalRePics=sizeof($rePicArray);
    if ($totalRePics > $reMaxPictures) $totalRePics = $reMaxPictures; 
}
if(trim($rePicArray[0])=="") $rePicArray[0]="images/no-image.png";

if($_SESSION["cladmin_settings"]["refriendlyurl"]=="enabled"){
$headline_slug=friendlyUrl($row0['headline']);    
$newTabLink=friendlyUrl($row0['category'],"_")."/".friendlyUrl($row0['subcategory'],"_")."/"."id-".$row0['id']."-".$region."-".$headline_slug;
}else  $newTabLink="index.php?ptype=viewFullListing&reid=".$row0['id'].$regionClause2;

$newTabButton="<a class='btn btn-sm btn-info pull-right' style='margin-top:5px;' target='_blank' href='$newTabLink'>".__("Direct Link")."</a>";
$row0['price']=number_format($row0['price']);

if($row0['price']!="" && $row0['price']!=0){
if($currency_before_price) $priceString="<b>".__("Price").":</b> ".$_SESSION["cladmin_settings"]["defaultcurrency"].$row0['price'].", ";
else $priceString="<b>".__("Price").":</b> ".$row0['price']." ".$_SESSION["cladmin_settings"]["defaultcurrency"].", ";    
} 

$listingLink="<a class='btn btn-sm btn-info pull-right' style='margin-top:5px; margin-right:5px;' href='#' id='theListingLink'><span id='".$row0['id'].$regionClause1."'>".__("More Info")."</span></a>";

if($count>0) $listingDelimeter="<hr />";

$style_attr=" style='display:block;' ";
$map_image="";    
foreach($rePicArray as $pid=>$pval){
if(trim($pval)!=""){        
$map_image=$map_image."<a rel='prettyPhoto[".$row0['id']."]' $style_attr href='$pval' ><img border='0' height='$thumbnailHeight' src='timthumb.php?h=$thumbnailHeight&src=$pval' /></a>";
$style_attr=" style='display:none;' ";
}  
}

/*
$markerContent=$markerContent.$listingDelimeter."<div class='markerInfo' $featuredClass $markerHeightStyle><h4 style='color:#000;'>".$smallHeadline."</h4><font style='font-size:85%;'><b>".__($row0['category'])." - ".__($row0['subcategory'])."</b> (#".$row0['id']."), $priceString<b>".__("Phone").":</b> ".$row0['contact_phone'].", <b>".__("Address").":</b> ".$row0['address'].", <b>".__("City").":</b> ".$row0['city']."</font><br /><div class='mapInfoText' style='float:left; width:70%; padding:10px 10px 0 0;'>$smallDescription $featuredTag<br />$newTabButton $listingLink</div>";

$markerContent=$markerContent."<div class='mapInfoPic'>$map_image</div></div>";
*/

$poptitle=nl2br(preg_replace( '/\s+/', ' ', escapeJavaScriptText($row0['headline'])));
$listingDelimeter="";
if(trim($row0['contact_phone'])!="") $phoneClause=__("Tel").": ".$row0['contact_phone']; else $phoneClause="";
if(trim($row0['vcardurl'])!="") $cardClause="<a class='popButtons rebutton btn btn-sm btn-primary' id='reSearch' href='".$row0['vcardurl']."' >".__("V Card")."</a>"; else $cardClause="";
if(trim($row0['pdfurl'])!="") $pdfClause="<a style='margin-left:10px;' class='popButtons rebutton btn btn-sm btn-primary' id='reSearch' href='".$row0['pdfurl']."' >".__("PDF")."</a>"; else $pdfClause="";
if(trim($row0['website'])!="") $websiteClause="<a style='margin-left:10px;' class='popButtons rebutton btn btn-sm btn-primary' id='reSearch' href='".$row0['website']."' >".__("Website")."</a>"; else $websiteClause="";
//$row0['address']=str_replace(',',"<br />", $row0['address']);

$amarkerContent[$row0['types']]="<div class='markerInfo' $featuredClass $markerHeightStyle><div class='popheading'>".$row0['subcategory']."</div><div class='poptitle'>".$poptitle."</div><div class='popactivities'>".$row0['activities']."</div><div class='popaddress'>".$row0['address']."<br />".$row0['postal']." ".$row0['city']."</div><div class='popphone'>".$phoneClause."</div><div class='popcomment'>".__("Contact").":<br />".$row0['description']."</div><br /><div class='popbuttons'>$cardClause $pdfClause $websiteClause</div></div>";
$count++;    
}

//return $markerContent;
//if($totalResults>1) $markerContent="<div style='height:200px;'>".$markerContent."</div>"; 
return json_encode($amarkerContent); 
  
}

/* Passes the data returned by getTextDataJson2 to registered filter hooks */
function getTextDataJson($nelatitude,$nelongitude,$swlatitude,$swlongitude,$pageNum){
 include_once("plugin_handler.inc.php"); 
 print call_plugin("sidebarTextResults",getTextDataJson2($nelatitude,$nelongitude,$swlatitude,$swlongitude,$pageNum));   
}

/* Retrieves sidebar text data as per search criteria */
function getTextDataJson2($nelatitude,$nelongitude,$swlatitude,$swlongitude,$pageNum){
include("config.php");
ob_start(); 
$con=mysql_connect($host,$username,$password) or die("Could not connect. Please try again.");
mysql_select_db($database,$con);
mysql_query("SET NAMES utf8");   

$alltextListings=array();
$boundListings=array();
$allmarkers=array();

if($listingsPerPage=="")$listingsPerPage=10;
if($pageNum=="" || $pageNum==0)$pageNum=1;

$startListingNum=($pageNum-1)*$listingsPerPage;
$endListingNum=$startListingNum+$listingsPerPage;
$startListingNum2=$startListingNum+1;

    $reCategory=$_GET['category'];
    $reSubcategories=$_GET['subcategories'];
    $activities=$_GET['activities'];
    $types=$_GET['types'];
    $reQuery=mysql_real_escape_string($_GET['requery']);
    $reCity=mysql_real_escape_string(trim($_GET['city']));
    $favorite=$_GET['favorite'];

 if($favorite==1){
       $allfavs=rtrim(str_replace(":", ",", $_SESSION["marked_reid"]),',');
       $qr="select id, latitude as la, longitude as lo, category, subcategory, headline, pictures, price, address, postal, city from $reListingTable where id IN ($allfavs);";
     }else{
$qr="select id, latitude as la, longitude as lo, category, subcategory, headline, pictures, price, address, postal, city, types from $reListingTable where listing_type <> 3  ".getRealValue($reCategory,"reCategory").getRealValue($reSubcategories,"reSubcategory").getRealValue($activities,"activities").getRealValue($types,"types").getRealValue($reQuery,"reQuery").getRealValue($reCity,"reCity").";";
     } 
$result=mysql_query($qr); 
//print "swlatitude=$swlatitude,<br />swlongitude=$swlongitude,<br />nelatitude=$nelatitude,<br />nelongitude=$nelongitude<br /><br />";
while($tlisting = mysql_fetch_assoc($result)){
 if(coordinate_in_bounds($swlatitude, $swlongitude, $nelatitude, $nelongitude, $tlisting['la'], $tlisting['lo'])){
  $boundListings[]=$tlisting;
 }
//print "<br />coordinates: $swlatitude, $swlongitude, $nelatitude, $nelongitude, <br /><br />".$tlisting['la'].", ".$tlisting['lo'];

}

$numResults=sizeof($boundListings);
$totalPages=ceil($numResults/$listingsPerPage);
if($pageNum>$totalPages)$pageNum=$totalPages;

$allTextListings="";
$thumbnailWidth=80;
$count=0;

if($endListingNum>$numResults)$endListingNum=$numResults;

//print "$startListingNum, $endListingNum, $numResults<br />";
if($numResults>0){
for($c=$startListingNum;$c<$endListingNum;$c++){
 if($count>=$listingsPerPage) break;
 $tlisting=$boundListings[$c];   
 $count++;
 
 $rePicArray=explode("::",$tlisting['pictures'],3); 
if(isset($rePicArray) && $rePicArray[0]!=""){
$picClause="<div class='textimage'><img src='timthumb.php?w=$thumbnailWidth&src=".$rePicArray[0]."' /></div>";
//$picClause="<div class='textimage'><img width='$thumbnailWidth' src='".$rePicArray[0]."' /></div>";
}else $picClause="<div class='textimage'><img width='$thumbnailWidth' src='images/no-image.png' /></div>";
$types=$tlisting['types'];

if($types=="Agence") $picClause="images/types_ineo/pink.png";
elseif($types=="Centre de travaux") $picClause="images/types_ineo/blue.png";
elseif($types=="Direction Déléguée") $picClause="images/types_ineo/voilet.png";
elseif($types=="Siege Social") $picClause="";
else $picClause="";

if($tlisting['price']>0){
    if($currency_before_price) $priceClause="<br /><span class='textcontent_price'>".__("Price").": $defaultCurrency ".$tlisting['price']."</span>"; 
    else $priceClause="<br /><span class='textcontent_price'>".__("Price").": ".$tlisting['price']." $defaultCurrency</span>";
} else $priceClause="";


$allTextListings="<div class='textrecord' id='textrecord-".$tlisting['id']."'><div class='textimage'><img src='$picClause' /></div><div class='textcontent'><span class='textcontent_headline'>".$tlisting['subcategory']."</span><br /><span class='textcontent_type'>".$tlisting['headline']."</span>".$tlisting['address']."<br />".$tlisting['postal']." ".$tlisting['city']."</div><span class='textlatlong' style='display:none;'>".$tlisting['la'].",".$tlisting['lo']."</span><span style='display:none;' class='texttype'>$types</span></div>".$allTextListings;

}
}

if($pageNum==1)$pgClassStart="disabled";
else $pgClassStart="pgNav";
if($pageNum==$totalPages)$pgClassEnd="disabled";
else $pgClassEnd="pgNav";
$nextPage=$pageNum+1;
$prevPage=$pageNum-1;

if($pageNum<5){
    $maxNumOfPagesInNavigation=5;
    $newFirstPage=1;
}else{
    $newFirstPage=$pageNum-2;
    $maxNumOfPagesInNavigation=$pageNum+2;
}

print $allTextListings;


if(strlen(trim($allTextListings)) > 0){
print "<table id='textResultsTable'><tr><td colspan='5'><div><ul class='pagination'>"; ?>
<li class='<?php print $pgClassStart; ?>'><a href='javascript: void(0)'  id='testResultLink-<?php print $prevPage; ?>'><?php print __("Previous");?></a></li>
<?php 
if($maxNumOfPagesInNavigation>$totalPages) $maxNumOfPagesInNavigation=$totalPages;
for($pg=$newFirstPage;$pg<=$maxNumOfPagesInNavigation;$pg++){
    if($pg!=$pageNum){
     ?>
    <li><a href='javascript: void(0)' class='pgNav' id='testResultLink-<?php print $pg; ?>'><?php print $pg; ?></a></li>
<?php }else{
     ?>
    <li class='active'><a href='javascript: void(0)' id='testResultLink-<?php print $pg; ?>'><?php print $pg; ?></a></li>
<?php } 
}
?>
<li class='<?php print "$pgClassEnd"; ?>'><a href='javascript: void(0)' id='testResultLink-<?php print $nextPage; ?>'><?php print __("Next");?></a></li></ul></div></td></tr>
<?php
print "</table>";
}else{
    print "<p style='margin-top:10px;text-align:center;'>".__("No results found").".</p>";
} 

return ob_get_clean();
}

/* Checks if supploed lat/lng are within the bound */
function coordinate_in_bounds($sw_lat, $sw_lng, $ne_lat, $ne_lng, $lat, $lng) {
    $inLng = false;
    $inLat = false;
    if($sw_lat > $ne_lat) $inLat = $lat > $ne_lat && $lat < $sw_lat;
    else $inLat = $lat < $ne_lat && $lat > $sw_lat;
    
    $inLng = $lng < $ne_lng && $lng > $sw_lng;
    return $inLat && $inLng;
    }

function getOodleListingsArray($reCategory,$reSubcategories,$reQuery,$reCity,$oodlecRegion,$reMaxPictures,$attr){
    include 'config.php';
    $oodleListingArray=array(); 
  if($cladmin_settings['oodleplugin']==1 && function_exists("getOodleArray")){
    if($oodlecRegion!="no"){
    list($price1,$price2)=explode("-", $rePrice);
    if($rePrice!=10) $attr="price_".$price1."_".$price2;
    $oodleSubcats=explode(",",$reSubcategories); 
    foreach($oodleSubcats as $oindex => $osubcatkey){ 
         if(trim($osubcatkey)!="") $oodleListingArray=array_merge($oodleListingArray,getOodleArray($reCategory,$osubcatkey,$reQuery,$reCity,$oodlecRegion,$reMaxPictures,1,50,$attr));
    }
    }
    }
   return $oodleListingArray;  
}

/* Escapes javascript for safe usage */
function escapeJavaScriptText($string)
{
	 return json_decode(str_replace("\u2029","",str_replace("\u2028", "", json_encode($string))));
}

/* Returns a part of the utf8 string */
function utf8_substr($str,$start)
{
	preg_match_all("/./u", $str, $ar);

	if(func_num_args() >= 3) {
		$end = func_get_arg(2);
		return join("",array_slice($ar[0],$start,$end));
	} else {
		return join("",array_slice($ar[0],$start));
	}
}

/* Returns entire listing data */
function getListingData($reid,$region=""){
	include("config.php");
	$allMarkedreid=explode(":",$_SESSION["marked_reid"]);
	$reCookieTime = 60 * 60 * 24 * 12 + time();
	list($tempw,$redomain)=explode(".",$_SERVER['HTTP_HOST'],2);
	setcookie('reidvisit', $_SESSION["reid"], $reCookieTime,"/", ".".$redomain, 1, true);
	
	if($region==""){
	$con=mysql_connect($host,$username,$password) or die("Could not connect. Please try again.");
	mysql_select_db($database,$con);
	mysql_query("SET NAMES utf8");
	$reid=mysql_real_escape_string($reid);
	$qr="select * from $reListingTable where id='$reid'"; 
	$result=mysql_query($qr);
	$row=mysql_fetch_assoc($result);
	}else{
		$combArray=convertArrayToClFormat(getOodleArray("","",$reid,"",$region,$reMaxPictures));
		$row=$combArray[0];
		$row['category']=ucfirst($row['category']);
	}
	return $row;	
}

/* Fetches the coordinates for an address */
function getLonglat2($address){
	define("MAPS_HOST", "maps.google.com");
	define("KEY", $googleMapAPIKey);
	$base_url = "http://" . MAPS_HOST . "/maps/geo?output=csv&key=" . KEY;
	$request_url = $base_url . "&q=" . urlencode($address);
	
	
	if(ini_get('allow_url_fopen') ) {
	$googleResult=file_get_contents($request_url);
	}else{
	if (!_iscurlinstalled()){ 
		echo "<p align='center'>cURL is NOT installed. Google geocoding won't work. Please ask your hosting provider to enable it.</p>";
	}else{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $request_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$googleResult = curl_exec ($ch);
		
	}
	}
    print $request_url."<br />$googleResult<br />";
	list($responseCode,$temp,$latitude,$longitude)=explode(",",$googleResult);
	return "$latitude::$longitude";
}

/* Fetches the coordinates for an address */
function getLongLat($address){
    $address=urlencode($address);
    $request_url = $base_url = "http://maps.googleapis.com/maps/api/geocode/json?address=$address&sensor=false";
    
    if(ini_get('allow_url_fopen') ) {
    $googleResult=file_get_contents($request_url);
    }else{
    if (!_iscurlinstalled()){ 
        echo "<p align='center'>cURL is NOT installed. Google geocoding won't work. Please ask your hosting provider to enable it.</p>";
    }else{
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $request_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $googleResult = curl_exec ($ch);
   }
    }
    $googlemap=json_decode($googleResult);
    if(!empty($googlemap)){
    foreach($googlemap->results as $res){
        $address = $res->geometry;
        $latlng = $address->location;
        $formattedaddress = $res->formatted_address;
    }
    }
 
    return $latlng->lat."::".$latlng->lng;
    
}

/* Checks if cURL is installed on the server */
function _iscurlinstalled() {
	if  (in_array  ('curl', get_loaded_extensions())) {
		return true;
	}
	else{
		return false;
	}
}

/* Shows image upload form after a listing has been added */
function getListingImageUploadForm($reid,$divid,$reimgid=""){
include("config.php");
$full_url_path = "http://" . $_SERVER['HTTP_HOST'] . preg_replace("#/[^/]*\.php$#simU", "/", $_SERVER["PHP_SELF"]);

?>
<div id='reimg<?php print $divid; ?>'>
<?php if(trim($reimgid)!=""){ list($temppart,$actualImgName)=explode("uploads/",$reimgid); ?>
<?php if($isThisDemo=="yes"){ ?>
<p align='center'><a class='deletepiclink' ><?php print $relanguage_tags["Delete Picture"]; ?></a></p><br />
<?php }else{ ?>
<p align='center'><a href='javascript: void(0)' onclick="infoResults('<?php print $reimgid.":::".$reid; ?>',12,'reimg<?php print $divid; ?>')" /><?php print $relanguage_tags["Delete Picture"]; ?></a></p><br />
<?php } ?>
 

<?php 
print "<img src='timthumb.php?w=250&src=$reimgid' width='250' border='0' />";
 } 
 
 ?>
</div>
<form action="ajaxupload.php" method="post" name="unobtrusive" id="unobtrusive" enctype="multipart/form-data">
<input type="hidden" name="maxSize" value="1024000" />
<input type="hidden" name="maxW" value="1200" />
<input type="hidden" name="fullPath" value="<?php print $full_url_path."uploads/"; ?>" />
<input type="hidden" name="relPath" value="uploads/" />
<input type="hidden" name="colorR" value="255" />
<input type="hidden" name="colorG" value="255" />
<input type="hidden" name="colorB" value="255" />
<input type="hidden" name="maxH" value="1000" />
<input type="hidden" name="filename" value="filename" />
<p><input type="file" name="filename" id="filename-<?php print $divid; ?>" class="refileup" value="filename" onchange="ajaxUpload(this.form,'ajaxupload.php?filename=filename&amp;maxSize=1024000&amp;maxW=1200&amp;fullPath=<?php print $full_url_path."uploads/"; ?>&amp;relPath=uploads/&amp;colorR=255&amp;colorG=255&amp;colorB=255&amp;maxH=1000&amp;reid=<?php print $reid; ?>&amp;reimgid=<?php print $reimgid; ?>&amp;repicid=<?php print $divid; ?>','reimg<?php print $divid; ?>','<?php print $relanguage_tags["File Uploading Please Wait"]; ?>...&lt;br /&gt;&lt;img src=\'images/loader_light_blue.gif\' width=\'128\' height=\'15\' border=\'0\' /&gt;','&lt;img src=\'images/error.gif\' width=\'16\' height=\'16\' border=\'0\' /&gt; <?php print $relanguage_tags["Error in upload"];?>.'); return false;" /></p>
<noscript><p><input type="submit" class='rebutton' name="submit" value="Upload Image" /></p></noscript>
</form>
<?php 	
//print $relanguage_tags["File Uploading Please Wait"]." - ".$relanguage_tags["File Uploading Please Wait"];
}

/* Sends email using phpmailer */
function sendReEmail($visitor_email,$msgbody,$to_email,$to_name,$subject,$show_response=true){
    include("config.php");
    if(is_ip_private($_SERVER['REMOTE_ADDR']) == false){
    if($gmailUsername!="" && $gmailPassword!=""){
    require_once("phpmailer/class.phpmailer.php");
    $mail = new PHPMailer();
    $mail->IsSMTP(); // send via SMTP
    $mail->SMTPAuth   = $SMTPAuth;                  // enable SMTP authentication
    $mail->CharSet = 'UTF-8';
    if($emaildebug=="yes") $mail->SMTPDebug = 1;
    if($resmtp=="smtp.gmail.com") $mail->SMTPSecure = "tls";                 // sets the prefix to the servier
    if($resmtpport==465) $mail->SMTPSecure = "ssl";
    $mail->Host       = $resmtp;      // sets GMAIL as the SMTP server
    $mail->Port       = $resmtpport;
    $mail->Username = $gmailUsername; // SMTP username
    $mail->Password = $gmailPassword; // SMTP password
    $mail->From = $gmailUsername;
    $mail->FromName = $reSiteName;
    $mail->AddAddress($to_email,$to_name);
    $mail->AddReplyTo($visitor_email,$reSiteName);
    $mail->WordWrap = 50; // set word wrap
    $mail->IsHTML(true); // send as HTML
    $mail->Subject = $subject;
    $mail->Body = $msgbody;
    
    if(!$mail->Send())
    {
    if($show_response) echo "<h3 align='center'>".$relanguage_tags["Message can not be sent"].": " . $mail->ErrorInfo."<br /><a href='javascript:history.go(-1);'>".$relanguage_tags["Go back"]."</a></h3>";
        //sendReEmail2($visitor_email,$msgbody,$to_email,$to_name,$subject);
    }
    else
    {
    if($show_response) echo "<h3 align='center'>".$relanguage_tags["Message has been sent"].". <a href='javascript:history.go(-1);'><b>".$relanguage_tags["Go back"]."</b></a></h3>";
    }
    }
 }else{
    if($show_response) print "<h3 align='center'>".__("Email can't be sent through localhost.")."</h3>";
 }  
}

/* Checks if it is a private IP. It is useful as email phpmailer function hangs on localhost */
function is_ip_private ($ip) {
    $pri_addrs = array (
                      '10.0.0.0|10.255.255.255', // single class A network
                      '172.16.0.0|172.31.255.255', // 16 contiguous class B network
                      '192.168.0.0|192.168.255.255', // 256 contiguous class C network
                      '169.254.0.0|169.254.255.255', // Link-local address also refered to as Automatic Private IP Addressing
                      '127.0.0.0|127.255.255.255' // localhost
                     );

    $long_ip = ip2long ($ip);
    if ($long_ip != -1) {
        foreach ($pri_addrs AS $pri_addr) {
            list ($start, $end) = explode('|', $pri_addr);
             if ($long_ip >= ip2long ($start) && $long_ip <= ip2long ($end)) return true;
        }
    }

    return false;
}


/* Sends email using sendmail */
function sendReEmail2($visitor_email,$msgbody,$to_email,$to_name,$subject,$show_response=true,$debug=""){
	include("config.php");

	require("phpmailer/class.phpmailer.php");
	$mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch
	$mail->IsSendmail(); // telling the class to use SendMail transport
	
	try {
	  $mail->AddReplyTo($visitor_email, $reSiteName);
	  $mail->AddAddress($to_email,$to_name);
	  $mail->SetFrom($gmailUsername, $reSiteName);
	  $mail->AddReplyTo($visitor_email, $reSiteName);
	  $mail->Subject = $subject;
	  $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!'; // optional - MsgHTML will create an alternate automatically
	  $mail->Body = $msgbody;
	  
	  if(!$mail->Send())
	  {
	  	if($show_response) echo "<h3 align='center'>".$relanguage_tags["Message can not be sent"].": " . $mail->ErrorInfo."<br /><a href='javascript:history.go(-1);'>".$relanguage_tags["Go back"]."</a></h3>";
	  }
	  else
	  {
	  	if($show_response) echo "<h3 align='center'>".$relanguage_tags["Message has been sent"].". <a href='javascript:history.go(-1);'><b>".$relanguage_tags["Go back"]."</b></a></h3>";
	  }
	  
	} catch (phpmailerException $e) {
	  echo "phpmailerException: ".$e->errorMessage(); //Pretty error messages from PHPMailer
	} catch (Exception $e) {
	  echo "Exception: ".$e->getMessage(); //Boring error messages from anything else!
	}
	

}

/* Add space between a very long string that can break formatting of a page */
function breakBigString($bigString,$maxStringLen){
	$descriptionArray=explode(" ",$bigString);
	$descriptionArraySize=sizeof($descriptionArray);
	$totalDescriptionLength=0;
	if($descriptionArraySize>1){
	for($j=0;$j<$descriptionArraySize;$j++){
		$descParts=round(strlen($descriptionArray[$j])/$maxStringLen);
		if(strlen($descriptionArray[$j])>$maxStringLen){
			for($i=1;$i<=$descParts;$i++){
				$splitAt=$i * $maxStringLen;
				$descriptionArray[$j]=substr_replace($descriptionArray[$j], " ", $splitAt, 0);
			}
			
		}
	}
	$bigString=implode(" ",$descriptionArray);
	}else{
		if(strlen($bigString)>$maxStringLen){
			$descParts=round(strlen($bigString)/$maxStringLen);
		
			for($i=1;$i<=$descParts;$i++){
					$splitAt=$i * $maxStringLen;
					$bigString=substr_replace($bigString, " ", $splitAt, 1);
				}
				
			
		}
	}
	
	return $bigString;
}

/* Function used to upload an image */
function uploadImage($mem_id){
	error_reporting(E_ALL & ~E_DEPRECATED & ~E_NOTICE);
	$full_url_path=dirname(__FILE__)."/";
	$path = $full_url_path."uploads/";

	$valid_formats = array("jpg", "png", "gif", "bmp","jpeg");
	if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
	{
		$name = $_FILES['photoimg']['name'];
		$size = $_FILES['photoimg']['size'];
		list($old_img_name,$old_ext)=explode(".",$name);
		if(strlen($name))
		{
			list($txt, $ext) = explode(".", $name);
			if(in_array($ext,$valid_formats))
			{
				if($size<(1024*1024)) // Image size max 1 MB
				{
					$actual_image_name = $old_img_name.time().$mem_id.".".$ext;
					$tmp = $_FILES['photoimg']['tmp_name'];
					if(move_uploaded_file($tmp, $path.$actual_image_name))
					{
						//mysql_query("update $rememberTable set photo='$actual_image_name' WHERE id='$mem_id'");
						return $actual_image_name;
						//echo "<img src='uploads/".$actual_image_name."' class='preview'>";
					}
					else
					echo "<p align='center'>Failed uploading image.</p>";
				}
				else
				echo "<p align='center'>Image file size max 1 MB</p>";
			}
			else
			echo "<p align='center'>Invalid file format..</p>";
		}
		else
		echo "<p align='center'>Please select image..!</p>";
		exit;

	}

}

/* Shows featured button */
function featuredButton($current_mem_id,$mem_id,$reid){
	include("config.php");
	//print "$current_mem_id, $mem_id".$_SESSION["memtype"];
	if($current_mem_id==$mem_id || $_SESSION["memtype"]==9){
		$full_url_path = "http://" . $_SERVER['HTTP_HOST'] . preg_replace("#/[^/]*\.php$#simU", "/", $_SERVER["PHP_SELF"]);
	?><br />
	<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
	<input type="hidden" name="cmd" value="_xclick">
	<input type="hidden" name="business" value="<?php print $ppemail; ?>">
	<input type="hidden" name="item_name" value="Featured Listing">
	<input type="hidden" name="item_number" size=30 value="<?php print $reid; ?>">
	<input type="hidden" name="amount" value="<?php print $featuredprice; ?>">
	<input type="hidden" name="currency_code" value="<?php print $ppcurrency; ?>">
	<input type="hidden" name="button_subtype" value="products">
	<input type="hidden" name="cn" value="Add special instructions to the seller">
	<input type="hidden" name="no_shipping" value="2">
	<input type="hidden" name="rm" value="1">
	<input type="hidden" name="return" value="<?php print $full_url_path; ?>">
	<input type="hidden" name="cancel_return" value="<?php print $full_url_path; ?>">
	<input type="hidden" name="bn" value="PP-BuyNowBF">
	<input  type="submit" class='btn btn-sm btn-primary' height='30' width='100' value='<?php print $relanguage_tags["Buy Featured"]; ?>' name="submit" alt="<?php print $relanguage_tags["Buy Featured"]; ?>">
	
	<?php 
	}
}

/* Replaces unwanted characters from a string so it can be safely used in a url */
function friendlyUrl($str = '',$replace='-') {

    $friendlyURL = htmlentities($str, ENT_COMPAT, "UTF-8", false); 
    $friendlyURL = preg_replace('/&([a-z]{1,2})(?:acute|lig|grave|ring|tilde|uml|cedil|caron);/i','\1',$friendlyURL);
    $friendlyURL = html_entity_decode($friendlyURL,ENT_COMPAT, "UTF-8"); 
    $friendlyURL = preg_replace('/[^a-z0-9-]+/i', $replace, $friendlyURL);
    $friendlyURL = preg_replace('/-+/', $replace, $friendlyURL);
    $friendlyURL = trim($friendlyURL, $replace);
    $friendlyURL = strtolower($friendlyURL);
    return $friendlyURL;

}

/* Remove them if magic_quotes are enabled on a server */
function remove_magic($array, $depth = 5)
{
	if($depth <= 0 || count($array) == 0)
	return $array;

	foreach($array as $key => $value)
	{
		if(is_array($value))
		$array[stripslashes($key)] = remove_magic($value, $depth - 1);
		else
		$array[stripslashes($key)] = stripslashes($value);
	}

	return $array;
}

/* Add extra html for the supplied page and outputs it */
function loadPage($filename){
	global $fullRelisting, $ptype, $memtype, $mem_id;
	include("config.php");
	?>
	<div class='row'>
	<div class='col-sm-4 col-md-4 col-lg-4'>
	<div id="sidebar">
	 <div id='sidebarLogin'>
     <span class='a_block'>
      <?php include("loginForm.php"); ?>
    <!-- <div class='ssocial'><a href='rss.php'><img border='0' src='images/rss.png' /></a></div> -->
     </span>
    </div>
	</div> <!-- end #sidebar -->
	</div>	  <!-- end span4 -->
	<div class='col-sm-8 col-md-8 col-lg-8'>  
	 <div id="mainContent">
	  <?php include($filename); ?>
	 </div> <!-- end #mainContent -->
	</div> <!-- end span8 -->
	</div> <!-- end row -->
			<br class="clearfloat" />
	<?php 
}

function printImageOfText($text){
	header("Content-type: image/png");
	if(isset($_GET['email'])){ 
	$text = $_GET['email'];

	if(isset($_GET['font'])) $font = $_GET['font']; 
	else $font = 4; //default
	$fontwidth  = imagefontwidth($font);
	$fontheight = imagefontheight($font);
	$border_x = $fontwidth*3;
	$border_y = $fontheight;
	$length = (strlen($text)*$fontwidth) + $border_x;
	$height = $fontheight + $border_y;
	$im = @ImageCreate ($length, $height) or die ("Could not create GD image stream");
	$background_color = ImageColorAllocate ($im, 255, 255, 0);
	$text_color = ImageColorAllocate ($im, 0, 0, 0);
	imagestring($im, $font,($border_x/2),($border_y/2),$text, $text_color);
	imagepng ($im);
	}
	
}

/* Ping the sitemap to major search engines. */
function pingSitemap(){
	$full_url_path = "http://" . $_SERVER['HTTP_HOST'] . preg_replace("#/[^/]*\.php$#simU", "/", $_SERVER["PHP_SELF"]);
	$sitemap = $full_url_path."sitemap.php";
	
	$pingurls = array(
			"http://www.google.com/webmasters/tools/ping?sitemap=",
			"http://submissions.ask.com/ping?sitemap=",
			"http://webmaster.live.com/ping.aspx?siteMap="
	);
	if (_iscurlinstalled()){
	foreach ($pingurls as $pingurl) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $pingurl.$sitemap);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$output = curl_exec($ch);
		curl_close($ch);
	}
	}
}

/* Get language short code for a language name */
function getLanguageName($langCode){
	$language="English";
	
	switch ($langCode){
		case "ar":
			$language="﻿Arabic";
			break;
		
		case "bg":
			$language="Bulgarian";
			break;
		
		case "ca":
			$language="Catalan";
			break;
		
		case "zh":
			$language="Chinese Simplified";
			break;
		
		case "cs":
			$language="Czech";
			break;
		
		case "da":
			$language="Danish";
			break;
		
		case "nl":
			$language="Dutch";
			break;
		
		case "en":
			$language="English";
			break;
		
		case "et":
			$language="Estonian";
			break;
		
		case "fi":
			$language="Finnish";
			break;
		
		case "fr":
			$language="French";
			break;
		
		case "de":
			$language="German";
			break;
		
		case "el":
			$language="Greek";
			break;
		
		case "ht":
			$language="Haitian Creole";
			break;
		
		case "he":
			$language="Hebrew";
			break;
		
		case "hi":
			$language="Hindi";
			break;
		
		case "hu":
			$language="Hungarian";
			break;
		
		case "id":
			$language="Indonesian";
			break;
		
		case "it":
			$language="Italian";
			break;
		
		case "ja":
			$language="Japanese";
			break;
		
		case "ko":
			$language="Korean";
			break;
		
		case "lv":
			$language="Latvian";
			break;
		
		case "lt":
			$language="Lithuanian";
			break;
		
		case "no":
			$language="Norwegian";
			break;
		
		case "pl":
			$language="Polish";
			break;
		
		case "pt":
			$language="Portuguese";
			break;
		
		case "ro":
			$language="Romanian";
			break;
		
		case "ru":
			$language="Russian";
			break;
		
		case "sk":
			$language="Slovak";
			break;
		
		case "sl":
			$language="Slovenian";
			break;
		
		case "es":
			$language="Spanish";
			break;
		
		case "sv":
			$language="Swedish";
			break;
		
		case "th":
			$language="Thai";
			break;
		
		case "tr":
			$language="Turkish";
			break;
		
		case "uk":
			$language="Ukrainian";
			break;
		
		case "vi":
			$language="Vietnamese";
			break;
	}
	return $language;
	
}

/* Checks if user is on mobile */
function isThisMobile(){
	$mobile_browser = '0';

	if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
		$mobile_browser++;
	}

	if ((strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') > 0) or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))) {
		$mobile_browser++;
	}

	$mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
	$mobile_agents = array(
			'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
			'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
			'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
			'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
			'newt','noki','oper','palm','pana','pant','phil','play','port','prox',
			'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
			'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
			'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
			'wapr','webc','winw','winw','xda ','xda-');

	if (in_array($mobile_ua,$mobile_agents)) {
		$mobile_browser++;
	}

	if (strpos(strtolower($_SERVER['ALL_HTTP']),'OperaMini')>0) {
		$mobile_browser++;
	}

	if (strpos(strtolower($_SERVER['ALL_HTTP']),'operamini')>0) {
		$mobile_browser++;
	}

	if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'windows') > 0) {
		$mobile_browser = 0;
	}

	if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'iemobile')>0) {
		$mobile_browser++;
	}

	if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),' ppc;')>0) {
		$mobile_browser++;
	}
	
	if ($mobile_browser > 0) {
		return true;
	}
	else {
		return false;
	}

}


?>